
package com.owncloud.java.lib.testclient;


public enum LoginMethod{
    UNKNOWN(LoginMethod.TEXT_UNKNOWN),
    DEFAULT(LoginMethod.TEXT_DEFAULT),
    ANONYMOUS(LoginMethod.TEXT_ANONYMOUS),
    TOKEN(LoginMethod.TEXT_TOKEN),
    SAML_SSO(LoginMethod.TEXT_SAML_SSO);
    
    private static final String TEXT_UNKNOWN="Unknown";
    private static final String TEXT_DEFAULT="Default";
    private static final String TEXT_ANONYMOUS="Anonymous";
    private static final String TEXT_TOKEN="Token";
    private static final String TEXT_SAML_SSO="Saml / Sso";
    
    private String mText;
    private LoginMethod(String text){
        mText= text;
    }
    
    @Override
    public String toString(){
        return mText;
    }
    
    public LoginMethod fromString(String method){
        LoginMethod ret= LoginMethod.UNKNOWN; 
        switch(method){
            case TEXT_DEFAULT:{ret= LoginMethod.DEFAULT;}break;
            case TEXT_ANONYMOUS:{ret= LoginMethod.ANONYMOUS;}break;
            case TEXT_TOKEN:{ret= LoginMethod.TOKEN;}break;
            case TEXT_SAML_SSO:{ret= LoginMethod.SAML_SSO;}break;
            default:
                ret= LoginMethod.UNKNOWN;
        }
        return ret;
    }
}