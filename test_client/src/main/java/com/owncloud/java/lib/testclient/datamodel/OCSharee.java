package com.owncloud.java.lib.testclient.datamodel;

import com.owncloud.java.lib.resources.shares.ShareType;

public class OCSharee implements Comparable<OCSharee>{
    
    public static final String JSON_KEY_LABEL= "label";
    public static final String JSON_KEY_VALUE= "value";
    public static final String JSON_KEY_SHAREWITH= "shareWith";
    public static final String JSON_KEY_SHARETYPE= "shareType";

    private String mLabel= "";
    private String mShareWith= "";
    private ShareType mShareType= ShareType.NO_SHARED;

    public OCSharee(){

    }

    public OCSharee(String label, String shareWith, ShareType shareType){
        mLabel= label;
        mShareWith= shareWith;
        mShareType= shareType;
    }

    @Override
    public String toString(){
        return mLabel;
    }

    @Override
    public int compareTo(OCSharee o){
        return mShareWith.compareTo((o != null) ? o.getShareWith() : "");
    }

    public String getLabel(){
        return mLabel;
    }

    public void setLabel(String label){
        mLabel= label;
    }

    public String getShareWith(){
        return mShareWith;
    }

    public void setShareWith(String shareWith){
        mShareWith= shareWith;
    }

    public ShareType getShareType(){
        return mShareType;
    }

    public void setShareType(ShareType shareType){
        mShareType= shareType;
    }
}
