package com.owncloud.java.lib.testclient;


public enum SharesDetailsMode{
    UNKNOWN(SharesDetailsMode.TEXT_UNKNOWN),
    SHARES(SharesDetailsMode.TEXT_SHARES),
    SHAREES(SharesDetailsMode.TEXT_SHAREES);
    
    private static final String TEXT_UNKNOWN="Unknown";
    private static final String TEXT_SHARES="Shares";
    private static final String TEXT_SHAREES="Sharees";
    
    private String mText;
    private SharesDetailsMode(String text){
        mText= text;
    }
    
    @Override
    public String toString(){
        return mText;
    }
    
    public SharesDetailsMode fromString(String method){
        SharesDetailsMode ret= SharesDetailsMode.UNKNOWN; 
        switch(method){
            case TEXT_SHARES:{ret= SharesDetailsMode.SHARES;}break;
            case TEXT_SHAREES:{ret= SharesDetailsMode.SHAREES;}break;
            default:
                ret= SharesDetailsMode.UNKNOWN;
        }
        return ret;
    }
}