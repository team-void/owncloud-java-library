package com.owncloud.java.lib.testclient;

import java.awt.EventQueue;

public class Bootstrap{

    public static void main(String[] args){
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run(){
                try{
                    TestApplication window= new TestApplication();
                    window.getFrame().setVisible(true);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

}
