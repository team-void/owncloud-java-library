package com.owncloud.java.lib.testclient;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import javax.activation.MimetypesFileTypeMap;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.json.JSONObject;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.owncloud.java.lib.common.OwnCloudClient;
import com.owncloud.java.lib.common.OwnCloudClientFactory;
import com.owncloud.java.lib.common.OwnCloudClientManagerFactory;
import com.owncloud.java.lib.common.OwnCloudClientManagerFactory.Policy;
import com.owncloud.java.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.java.lib.common.network.NetworkUtils;
import com.owncloud.java.lib.common.network.OnDatatransferProgressListener;
import com.owncloud.java.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.java.lib.common.operations.RemoteOperation;
import com.owncloud.java.lib.common.operations.RemoteOperationResult;
import com.owncloud.java.lib.common.utils.Log_OC;
import com.owncloud.java.lib.resources.files.ChunkedUploadRemoteFileOperation;
import com.owncloud.java.lib.resources.files.CopyRemoteFileOperation;
import com.owncloud.java.lib.resources.files.CreateRemoteFolderOperation;
import com.owncloud.java.lib.resources.files.DownloadRemoteFileOperation;
import com.owncloud.java.lib.resources.files.ExistenceCheckRemoteOperation;
import com.owncloud.java.lib.resources.files.FileUtils;
import com.owncloud.java.lib.resources.files.MoveRemoteFileOperation;
import com.owncloud.java.lib.resources.files.ReadRemoteFileOperation;
import com.owncloud.java.lib.resources.files.ReadRemoteFolderOperation;
import com.owncloud.java.lib.resources.files.RemoteFile;
import com.owncloud.java.lib.resources.files.RemoveRemoteFileOperation;
import com.owncloud.java.lib.resources.files.RenameRemoteFileOperation;
import com.owncloud.java.lib.resources.files.UploadRemoteFileOperation;
import com.owncloud.java.lib.resources.shares.CreateRemoteShareOperation;
import com.owncloud.java.lib.resources.shares.GetRemoteShareOperation;
import com.owncloud.java.lib.resources.shares.GetRemoteShareesOperation;
import com.owncloud.java.lib.resources.shares.GetRemoteSharesForFileOperation;
import com.owncloud.java.lib.resources.shares.GetRemoteSharesOperation;
import com.owncloud.java.lib.resources.shares.OCShare;
import com.owncloud.java.lib.resources.shares.RemoveRemoteShareOperation;
import com.owncloud.java.lib.resources.shares.ShareType;
import com.owncloud.java.lib.resources.shares.UpdateRemoteShareOperation;
import com.owncloud.java.lib.resources.status.GetRemoteCapabilitiesOperation;
import com.owncloud.java.lib.resources.status.GetRemoteStatusOperation;
import com.owncloud.java.lib.resources.status.OCCapability;
import com.owncloud.java.lib.resources.status.OwnCloudVersion;
import com.owncloud.java.lib.resources.users.GetRemoteUserAvatarOperation;
import com.owncloud.java.lib.resources.users.GetRemoteUserAvatarOperation.ResultData;
import com.owncloud.java.lib.resources.users.GetRemoteUserInfoOperation;
import com.owncloud.java.lib.resources.users.GetRemoteUserInfoOperation.UserInfo;
import com.owncloud.java.lib.resources.users.GetRemoteUserQuotaOperation;
import com.owncloud.java.lib.resources.users.GetRemoteUserQuotaOperation.Quota;
import com.owncloud.java.lib.testclient.datamodel.OCSharee;

/**
 * Sample Swing App to test OC framework
 * @author Diego F. Lassa
 *
 * Based on the Android class version, from the authors:\
 * @author masensio
 * @author David A. Velascocreatef
 */
class TestApplication implements OnRemoteOperationListener, OnDatatransferProgressListener{

    private JFrame mFrmDSsync;

    private static final int BUFFER_SIZE= 1024;

    private ThreadPoolExecutor mExecutor= (ThreadPoolExecutor) Executors.newCachedThreadPool();
    private JFileChooser mDirectoryChooser= new JFileChooser();
    private JFileChooser mFileToUploadChooser= new JFileChooser();
    private Map<String, Boolean> mFilesTreeLoadedNodes= new HashMap<>();
    private Map<String, Boolean> mSharesTreeLoadedNodes= new HashMap<>();
    private Set<OCTreeElements> mFilesTreeElements= new HashSet<>(Arrays.asList(OCTreeElements.DIRECTORY, OCTreeElements.FILE));
    private Set<OCTreeElements> mSharesTreeElements= new HashSet<>(Arrays.asList(OCTreeElements.DIRECTORY, OCTreeElements.FILE));
    private List<Object> mSharesMasterObjects= new ArrayList<>();

    private OwnCloudClient mClient;
    private JTextField mTxtFldUserAgent;
    private JTextField mTxtFldServerBaseURL;
    private JTextField mTxtFldUser;
    private JPasswordField mPassword;
    private JComboBox<LoginMethod> mCmbBxLoginMethod;
    private JButton mBtnApplyCredentials;
    private JButton mBtnRevokeCredentials;
    private JList<String> mLstUserData;
    private JLabel mLblUserAvatar;
    private JList<String> mLstServerInfo;
    private JList<String> mLstSharesMaster;
    private JComboBox<ShareType> mCmbBxSharesCreateShareType;
    private JTextField mTxtFldSharesCreatePassword;
    private JCheckBox mChckBxSharesCreateIsPublic;
    private JCheckBox mChckBxSharesCreatePermissionCreate;
    private JCheckBox mChckBxSharesCreatePermissionRead;
    private JCheckBox mChckBxSharesCreatePermissionUpdate;
    private JCheckBox mChckBxSharesCreatePermissionDelete;
    private JCheckBox mChckBxSharesCreatePermissionReShare;
    private JList<String> mLstLog;
    private JButton mBtnSharesCreateShare;
    private JCheckBox mChckBxUseAsynchronousAPICalls;
    private JTextField mTxtFldSyncDirectory;
    private JButton mBtnSelectSyncDirectory;
    private JButton mBtnRefreshFilesTreeFiles;
    private JTextField mTxtFldNewDirectoryName;
    private JButton mBtnCreateDirectory;
    private JCheckBox mChckBxCreateFullPath;
    private JTree mFilesTreeFiles;
    private JButton mBtnGetUserInfo;
    private JButton mBtnGetUserAvatar;
    private JButton mBtnGetUserQuota;
    private JButton mBtnGetServerStatus;
    private JButton mBtnGetServerCapabilities;
    private JButton mBtnRemoveFileOrDir;
    private JTextField mTxtFldUploadFile;
    private JButton mBtnSelectFileToUpload;
    private JButton mBtnUploadFile;
    private JTextField mTxtFldRemoteFilePathToDownload;
    private JButton mBtnDownloadFile;
    private JTabbedPane mTbdPnActions;
    private JTextField mTxtFldFileOrDirToBeRenamed;
    private JTextField mTxtFldFileOrDirNewName;
    private JButton mBtnRenameFileOrDir;
    private JTextField mTxtFldExistenceCheck;
    private JButton mBtnExistenceCheck;
    private JCheckBox mChckBxSuccessIfAbsent;
    private JCheckBox mChckBxHideFilesTreeFiles;
    private JButton mBtnReadInfo;
    private JTextField mTxtFldSharesFileToGetShares;
    private JButton mBtnGetSharesForFile;
    private JTextField mTxtFldSourceFileOrDirForCopy;
    private JTextField mTxtFldDestinyPathForCopy;
    private JButton mBtnCopyFileOrDir;
    private JCheckBox mChckBxGetCopySourceFromTree;
    private JCheckBox mChckBxGetCopyDestinyFromTree;
    private JButton mBtnMoveFileOrDir;
    private JTextField mTxtFldSourceFileOrDirForMove;
    private JTextField mTxtFldDestinyPathForMove;
    private JCheckBox mChckBxGetMoveSourceFromTree;
    private JLabel lblDestinyPathForMove;
    private Component horizontalGlueDestinyPathForMove;
    private JCheckBox mChckBxGetMoveDestinyFromTree;
    private JButton mBtnChunkedUploadFile;
    private JTextField mTxtFldChunkedUploadFile;
    private JButton mBtnSelectFileToChunkUpload;
    private JTree mSharesTreeFiles;
    private JList<String> mLstShareDetail;
    private JCheckBox mChckBxHideSharesTreeFiles;
    private JButton mBtnRefreshSharesTreeFiles;
    private JComboBox<SharesDetailsMode> mCmbBxSharesDetailsMode;
    private JButton mBtnListSharees;
    private JTextField mTxtFldShareesSearchString;
    private JSpinner mSpnrShareesPageNumber;
    private JSpinner mSpnrShareesItensPerPage;
    private JButton mBtnListShares;
    private JCheckBox mChckbGetSharesForFilesReshares;
    private JCheckBox mChckbGetSharesForFilesSubfiles;
    private JButton mBtnSharesGetShare;
    private JSpinner mSpinnerSharesRemoteShareIdToGet;
    private JButton mBtnSharesRemoveShare;
    private JSpinner mSpinnerSharesRemoteShareIdToRemove;
    private JTextField mTxtFldSharesFileToBeShared;
    private JComboBox<OCSharee> mCmbBxSharesCreateSharees;
    private JTextField mTxtFldSharesPathToUpdate;
    private JTextField mTxtFldSharesUpdatePassword;
    private JTextField mTxtFldSharesUpdateExpirationDate;
    private JSpinner mSpinnerSharesRemoteIdToUpdate;
    private JCheckBox mChckBxSharesUpdatePermissionRead;
    private JCheckBox mChckBxSharesUpdatePermissionUpdate;
    private JCheckBox mChckBxSharesUpdatePermissionCreate;
    private JCheckBox mChckBxSharesUpdatePermissionDelete;
    private JCheckBox mChckBxSharesUpdatePermissionReShare;
    private JCheckBox mChckBxSharesUpdateIsPublic;
    private JButton mBtnSharesUpdateShare;

    /**
     * Launch the application.
     */
    public static void main(String[] args){
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run(){
                try{
                    TestApplication window= new TestApplication();
                    window.mFrmDSsync.setVisible(true);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public JFrame getFrame(){
        return mFrmDSsync;
    }

    /**
     * Create the application.
     */
    public TestApplication(){
        // Set folder for store logs
        Log_OC.setLogDataFolder("./");
        Log_OC.startLogging("./");
        Log_OC.d("Debug", "start logging");

        initializeUI();
        initializeBindEvents();
        initializeLibrary();
        initializeHTTPClient();
    }

    private void initializeLibrary(){
        boolean isSamlAuth= false;// AUTH_ON.equals(getString(R.string.auth_method_saml_web_sso));
        OwnCloudClientManagerFactory.setUserAgent(mTxtFldUserAgent.getText());
        if(isSamlAuth)
            OwnCloudClientManagerFactory.setDefaultPolicy(Policy.SINGLE_SESSION_PER_ACCOUNT);
        else
            OwnCloudClientManagerFactory.setDefaultPolicy(Policy.ALWAYS_NEW_CLIENT);
    }

    /**
     * Initialize the contents of the frame.
     * @wbp.parser.entryPoint
     */
    private void initializeUI(){
        mFrmDSsync= new JFrame();
        mFrmDSsync.setName("mFrmMain");
        mFrmDSsync.setTitle("Test Application for the ownCloud Java Library");
        mFrmDSsync.setBounds(100, 100, 579, 620);
        mFrmDSsync.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mFrmDSsync.getContentPane().setLayout(new BorderLayout(0, 0));

        mDirectoryChooser.setCurrentDirectory(new java.io.File("."));
        mDirectoryChooser.setDialogTitle("Sync Directory");
        mDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        mDirectoryChooser.setAcceptAllFileFilterUsed(false);

        mFileToUploadChooser.setCurrentDirectory(new java.io.File("."));
        mFileToUploadChooser.setDialogTitle("Select File");
        mFileToUploadChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        mFileToUploadChooser.setAcceptAllFileFilterUsed(true);

        JSplitPane splitPane= new JSplitPane();
        splitPane.setDividerSize(8);
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        Double spliterPos= mFrmDSsync.getHeight() * 0.75;
        splitPane.setDividerLocation(spliterPos.intValue());
        mFrmDSsync.getContentPane().add(splitPane, BorderLayout.CENTER);

        JTabbedPane tabbedPaneMain= new JTabbedPane(JTabbedPane.TOP);
        splitPane.setLeftComponent(tabbedPaneMain);

        JPanel pnlConfiguration= new JPanel();
        tabbedPaneMain.addTab("Configurations", null, pnlConfiguration, null);
        pnlConfiguration.setLayout(new BorderLayout(0, 0));

        JTabbedPane tbdPnConfiguration= new JTabbedPane(JTabbedPane.TOP);
        pnlConfiguration.add(tbdPnConfiguration);

        JPanel pnlConfigurarionServer= new JPanel();
        tbdPnConfiguration.addTab("Server", null, pnlConfigurarionServer, null);
        pnlConfigurarionServer.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        JLabel labelUserAgent= new JLabel("User Agent");
        labelUserAgent.setFont(new Font("Tahoma", Font.PLAIN, 18));
        pnlConfigurarionServer.add(labelUserAgent, "2, 2");

        mTxtFldUserAgent= new JTextField();
        mTxtFldUserAgent.setText("Mozilla/5.0 (Android) ownCloud-java/0.0.1-SNAPSHOT");
        mTxtFldUserAgent.setFont(new Font("Tahoma", Font.PLAIN, 15));
        mTxtFldUserAgent.setColumns(10);
        pnlConfigurarionServer.add(mTxtFldUserAgent, "2, 4, fill, default");

        JLabel labelServerBaseURL= new JLabel("ownCloud Server Base URL");
        labelServerBaseURL.setFont(new Font("Tahoma", Font.PLAIN, 18));
        pnlConfigurarionServer.add(labelServerBaseURL, "2, 8");

        mTxtFldServerBaseURL= new JTextField();
        mTxtFldServerBaseURL.setText("http://owncloud.datasafer.com.br");
        mTxtFldServerBaseURL.setFont(new Font("Tahoma", Font.PLAIN, 15));
        mTxtFldServerBaseURL.setColumns(10);
        pnlConfigurarionServer.add(mTxtFldServerBaseURL, "2, 10, fill, default");

        JPanel pnlConfigurarionCredentials= new JPanel();
        tbdPnConfiguration.addTab("Credentials", null, pnlConfigurarionCredentials, null);
        pnlConfigurarionCredentials.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        JLabel labelUser= new JLabel("User");
        labelUser.setFont(new Font("Tahoma", Font.PLAIN, 18));
        pnlConfigurarionCredentials.add(labelUser, "2, 2");

        mTxtFldUser= new JTextField();
        mTxtFldUser.setText("diego.lassa");
        mTxtFldUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
        mTxtFldUser.setColumns(10);
        pnlConfigurarionCredentials.add(mTxtFldUser, "2, 4, fill, default");

        JLabel labelPassword= new JLabel("Password");
        labelPassword.setFont(new Font("Tahoma", Font.PLAIN, 18));
        pnlConfigurarionCredentials.add(labelPassword, "2, 8");

        mPassword= new JPasswordField("zaq1ZAQ!");
        mPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
        mPassword.setColumns(10);
        pnlConfigurarionCredentials.add(mPassword, "2, 10, fill, default");

        JLabel labelLoginMethod= new JLabel("Method");
        labelLoginMethod.setFont(new Font("Tahoma", Font.PLAIN, 18));
        pnlConfigurarionCredentials.add(labelLoginMethod, "2, 14");

        mCmbBxLoginMethod= new JComboBox<>();
        mCmbBxLoginMethod.setFont(new Font("Tahoma", Font.PLAIN, 15));
        LoginMethod loginMethod[]= new LoginMethod[4];
        loginMethod[0]= LoginMethod.DEFAULT;
        loginMethod[1]= LoginMethod.ANONYMOUS;
        loginMethod[2]= LoginMethod.TOKEN;
        loginMethod[3]= LoginMethod.SAML_SSO;
        mCmbBxLoginMethod.setModel(new DefaultComboBoxModel<>(loginMethod));
        pnlConfigurarionCredentials.add(mCmbBxLoginMethod, "2, 16, fill, default");

        Box horizontalBoxCredentialsButtons= Box.createHorizontalBox();
        pnlConfigurarionCredentials.add(horizontalBoxCredentialsButtons, "2, 20");

        mBtnApplyCredentials= new JButton("Apply Credentials");

        horizontalBoxCredentialsButtons.add(mBtnApplyCredentials);

        Component rigidArea= Box.createRigidArea(new Dimension(20, 20));
        horizontalBoxCredentialsButtons.add(rigidArea);

        mBtnRevokeCredentials= new JButton("Revoke Credentials");

        horizontalBoxCredentialsButtons.add(mBtnRevokeCredentials);

        JPanel pnlConfigurarionMisc= new JPanel();
        tbdPnConfiguration.addTab("Misc", null, pnlConfigurarionMisc, null);
        pnlConfigurarionMisc.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        mChckBxUseAsynchronousAPICalls= new JCheckBox("Use Asynchronous API Calls");
        pnlConfigurarionMisc.add(mChckBxUseAsynchronousAPICalls, "2, 2");

        JLabel lblSyncDirectory= new JLabel("Sync Directory");
        pnlConfigurarionMisc.add(lblSyncDirectory, "2, 6");

        Box horizontalBoxSyncDirectory= Box.createHorizontalBox();
        pnlConfigurarionMisc.add(horizontalBoxSyncDirectory, "2, 8");

        mTxtFldSyncDirectory= new JTextField();
        horizontalBoxSyncDirectory.add(mTxtFldSyncDirectory);
        mTxtFldSyncDirectory.setText(".\\SyncDir.Test");
        mTxtFldSyncDirectory.setColumns(10);

        mBtnSelectSyncDirectory= new JButton("...");
        horizontalBoxSyncDirectory.add(mBtnSelectSyncDirectory);

        JPanel pnlActions= new JPanel();
        tabbedPaneMain.addTab("API Calls", null, pnlActions, null);
        pnlActions.setLayout(new BorderLayout(0, 0));

        mTbdPnActions= new JTabbedPane(JTabbedPane.LEFT);
        pnlActions.add(mTbdPnActions, BorderLayout.CENTER);

        JPanel pnlFiles= new JPanel();
        mTbdPnActions.addTab("Files", null, pnlFiles, null);
        pnlFiles.setLayout(new BorderLayout(0, 0));

        Box horizontalBoxFilesTreeActions= Box.createHorizontalBox();
        pnlFiles.add(horizontalBoxFilesTreeActions, BorderLayout.NORTH);

        Component horizontalGlueFilesTreeOptions= Box.createHorizontalGlue();
        horizontalBoxFilesTreeActions.add(horizontalGlueFilesTreeOptions);

        mChckBxHideFilesTreeFiles= new JCheckBox("Hide Files");
        horizontalBoxFilesTreeActions.add(mChckBxHideFilesTreeFiles);

        mBtnRefreshFilesTreeFiles= new JButton("Refresh");
        horizontalBoxFilesTreeActions.add(mBtnRefreshFilesTreeFiles);

        JScrollPane scrollPaneFilesTreeFiles= new JScrollPane();
        pnlFiles.add(scrollPaneFilesTreeFiles, BorderLayout.CENTER);

        mFilesTreeFiles= new JTree();
        mFilesTreeFiles.setModel(new DefaultTreeModel(null));
        scrollPaneFilesTreeFiles.setViewportView(mFilesTreeFiles);

        JTabbedPane tbdPnFilesActions= new JTabbedPane(JTabbedPane.TOP);
        pnlFiles.add(tbdPnFilesActions, BorderLayout.SOUTH);

        JPanel pnlCreateDir= new JPanel();
        tbdPnFilesActions.addTab("Create Dir", null, pnlCreateDir, null);
        pnlCreateDir.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        Box horizontalBoxCreateFolder= Box.createHorizontalBox();
        pnlCreateDir.add(horizontalBoxCreateFolder, "2, 2");

        mTxtFldNewDirectoryName= new JTextField();
        horizontalBoxCreateFolder.add(mTxtFldNewDirectoryName);
        mTxtFldNewDirectoryName.setColumns(10);

        mBtnCreateDirectory= new JButton("Create");
        horizontalBoxCreateFolder.add(mBtnCreateDirectory);

        mChckBxCreateFullPath= new JCheckBox("Create Full Path");
        mChckBxCreateFullPath.setSelected(true);
        pnlCreateDir.add(mChckBxCreateFullPath, "2, 4");

        JPanel pnlRemoveFileOrDir= new JPanel();
        tbdPnFilesActions.addTab("Remove", null, pnlRemoveFileOrDir, null);
        pnlRemoveFileOrDir.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        mBtnRemoveFileOrDir= new JButton("Delete");
        pnlRemoveFileOrDir.add(mBtnRemoveFileOrDir, "2, 2");

        JPanel pnlReadInfo= new JPanel();
        tbdPnFilesActions.addTab("Read Info", null, pnlReadInfo, null);
        pnlReadInfo.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        mBtnReadInfo= new JButton("Read Info");

        pnlReadInfo.add(mBtnReadInfo, "2, 2");

        JPanel pnlCheckExistence= new JPanel();
        tbdPnFilesActions.addTab("Check", null, pnlCheckExistence, null);
        pnlCheckExistence.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        JLabel lblCheckExistence= new JLabel("Path to check");
        pnlCheckExistence.add(lblCheckExistence, "2, 2");

        mTxtFldExistenceCheck= new JTextField();
        pnlCheckExistence.add(mTxtFldExistenceCheck, "2, 4, fill, default");
        mTxtFldExistenceCheck.setColumns(10);

        mChckBxSuccessIfAbsent= new JCheckBox("Success If Absent");
        pnlCheckExistence.add(mChckBxSuccessIfAbsent, "2, 6");

        mBtnExistenceCheck= new JButton("Check");

        pnlCheckExistence.add(mBtnExistenceCheck, "2, 8");

        JPanel pnlUploadFile= new JPanel();
        tbdPnFilesActions.addTab("Upload", null, pnlUploadFile, null);
        pnlUploadFile.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        Box horizontalBoxFilesActions= Box.createHorizontalBox();
        pnlUploadFile.add(horizontalBoxFilesActions, "2, 2");

        mTxtFldUploadFile= new JTextField();
        horizontalBoxFilesActions.add(mTxtFldUploadFile);
        mTxtFldUploadFile.setColumns(10);

        mBtnSelectFileToUpload= new JButton("...");
        horizontalBoxFilesActions.add(mBtnSelectFileToUpload);

        mBtnUploadFile= new JButton("Upload");
        pnlUploadFile.add(mBtnUploadFile, "2, 4");

        JPanel pnlChunkedUpload= new JPanel();
        tbdPnFilesActions.addTab("Chunked Upload", null, pnlChunkedUpload, null);
        pnlChunkedUpload.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        Box horizontalBoxChunkedUpload= Box.createHorizontalBox();
        pnlChunkedUpload.add(horizontalBoxChunkedUpload, "2, 2");

        mTxtFldChunkedUploadFile= new JTextField();
        horizontalBoxChunkedUpload.add(mTxtFldChunkedUploadFile);
        mTxtFldChunkedUploadFile.setColumns(10);

        mBtnSelectFileToChunkUpload= new JButton("...");
        horizontalBoxChunkedUpload.add(mBtnSelectFileToChunkUpload);

        mBtnChunkedUploadFile= new JButton("Chunk Upload");
        pnlChunkedUpload.add(mBtnChunkedUploadFile, "2, 4");

        JPanel pnlDownloadFile= new JPanel();
        tbdPnFilesActions.addTab("Download", null, pnlDownloadFile, null);
        pnlDownloadFile.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        Box horizontalBoxDownloadFile= Box.createHorizontalBox();
        pnlDownloadFile.add(horizontalBoxDownloadFile, "2, 2");

        mTxtFldRemoteFilePathToDownload= new JTextField();
        mTxtFldRemoteFilePathToDownload.setEnabled(false);
        horizontalBoxDownloadFile.add(mTxtFldRemoteFilePathToDownload);
        mTxtFldRemoteFilePathToDownload.setColumns(10);

        mBtnDownloadFile= new JButton("Download");
        horizontalBoxDownloadFile.add(mBtnDownloadFile);

        JPanel pnlRenameFileOrDir= new JPanel();
        tbdPnFilesActions.addTab("Rename", null, pnlRenameFileOrDir, null);
        pnlRenameFileOrDir.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        JLabel lblFileOrDirToBeRenamed= new JLabel("File or Dir to be renamed");
        pnlRenameFileOrDir.add(lblFileOrDirToBeRenamed, "2, 2");

        mTxtFldFileOrDirToBeRenamed= new JTextField();
        mTxtFldFileOrDirToBeRenamed.setEnabled(false);
        pnlRenameFileOrDir.add(mTxtFldFileOrDirToBeRenamed, "2, 4, fill, default");
        mTxtFldFileOrDirToBeRenamed.setColumns(10);

        JLabel lblFileNewName= new JLabel("New Name");
        pnlRenameFileOrDir.add(lblFileNewName, "2, 6");

        mTxtFldFileOrDirNewName= new JTextField();
        pnlRenameFileOrDir.add(mTxtFldFileOrDirNewName, "2, 8, fill, default");
        mTxtFldFileOrDirNewName.setColumns(10);

        mBtnRenameFileOrDir= new JButton("Rename");
        pnlRenameFileOrDir.add(mBtnRenameFileOrDir, "2, 10, default, center");

        JPanel pnlCopyFileOrFolder= new JPanel();
        tbdPnFilesActions.addTab("Copy", null, pnlCopyFileOrFolder, null);
        pnlCopyFileOrFolder.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        Box horizontalBoxSourceFileOrDirCopy= Box.createHorizontalBox();
        pnlCopyFileOrFolder.add(horizontalBoxSourceFileOrDirCopy, "2, 2");

        JLabel lblSourceFileOrDirForCopy= new JLabel("Source Item");
        horizontalBoxSourceFileOrDirCopy.add(lblSourceFileOrDirForCopy);

        Component horizontalGlueSourceFileOrDirForCopy= Box.createHorizontalGlue();
        horizontalBoxSourceFileOrDirCopy.add(horizontalGlueSourceFileOrDirForCopy);

        mChckBxGetCopySourceFromTree= new JCheckBox("Get Tree Selection");
        mChckBxGetCopySourceFromTree.setSelected(true);
        horizontalBoxSourceFileOrDirCopy.add(mChckBxGetCopySourceFromTree);

        mTxtFldSourceFileOrDirForCopy= new JTextField();
        mTxtFldSourceFileOrDirForCopy.setEnabled(false);
        pnlCopyFileOrFolder.add(mTxtFldSourceFileOrDirForCopy, "2, 4, fill, default");
        mTxtFldSourceFileOrDirForCopy.setColumns(10);

        Box horizontalBoxDestinyCopyPath= Box.createHorizontalBox();
        pnlCopyFileOrFolder.add(horizontalBoxDestinyCopyPath, "2, 6");

        JLabel lblDestinyPathForCopy= new JLabel("Destiny Path");
        horizontalBoxDestinyCopyPath.add(lblDestinyPathForCopy);

        Component horizontalGlueDestinyPathForCopy= Box.createHorizontalGlue();
        horizontalBoxDestinyCopyPath.add(horizontalGlueDestinyPathForCopy);

        mChckBxGetCopyDestinyFromTree= new JCheckBox("Get Tree Selection");
        mChckBxGetCopyDestinyFromTree.setSelected(true);
        horizontalBoxDestinyCopyPath.add(mChckBxGetCopyDestinyFromTree);

        mTxtFldDestinyPathForCopy= new JTextField();
        mTxtFldDestinyPathForCopy.setEnabled(false);
        pnlCopyFileOrFolder.add(mTxtFldDestinyPathForCopy, "2, 8, fill, default");
        mTxtFldDestinyPathForCopy.setColumns(10);

        mBtnCopyFileOrDir= new JButton("Copy");

        pnlCopyFileOrFolder.add(mBtnCopyFileOrDir, "2, 10");

        JPanel pnlMoveFileOrFolder= new JPanel();
        tbdPnFilesActions.addTab("Move", null, pnlMoveFileOrFolder, null);
        pnlMoveFileOrFolder.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        Box horizontalBoxSourceFileOrDirMove= Box.createHorizontalBox();
        pnlMoveFileOrFolder.add(horizontalBoxSourceFileOrDirMove, "2, 2");

        JLabel lblSourceFileOrDirForMove= new JLabel("Source Item");
        horizontalBoxSourceFileOrDirMove.add(lblSourceFileOrDirForMove);

        Component horizontalGlueSourceFileOrDirForMove= Box.createHorizontalGlue();
        horizontalBoxSourceFileOrDirMove.add(horizontalGlueSourceFileOrDirForMove);

        mChckBxGetMoveSourceFromTree= new JCheckBox("Get Tree Selection");
        horizontalBoxSourceFileOrDirMove.add(mChckBxGetMoveSourceFromTree);

        mTxtFldSourceFileOrDirForMove= new JTextField();
        pnlMoveFileOrFolder.add(mTxtFldSourceFileOrDirForMove, "2, 4, fill, default");
        mTxtFldSourceFileOrDirForMove.setColumns(10);

        Box horizontalBoxDestinyMoverPath= Box.createHorizontalBox();
        pnlMoveFileOrFolder.add(horizontalBoxDestinyMoverPath, "2, 6");

        lblDestinyPathForMove= new JLabel("Destiny Path");
        horizontalBoxDestinyMoverPath.add(lblDestinyPathForMove);

        horizontalGlueDestinyPathForMove= Box.createHorizontalGlue();
        horizontalBoxDestinyMoverPath.add(horizontalGlueDestinyPathForMove);

        mChckBxGetMoveDestinyFromTree= new JCheckBox("Get Tree Selection");
        horizontalBoxDestinyMoverPath.add(mChckBxGetMoveDestinyFromTree);

        mTxtFldDestinyPathForMove= new JTextField();
        pnlMoveFileOrFolder.add(mTxtFldDestinyPathForMove, "2, 8, fill, default");
        mTxtFldDestinyPathForMove.setColumns(10);

        mBtnMoveFileOrDir= new JButton("Move");

        pnlMoveFileOrFolder.add(mBtnMoveFileOrDir, "2, 10");

        JPanel pnlShares= new JPanel();
        mTbdPnActions.addTab("Shares", null, pnlShares, null);

        JSplitPane splitPaneSharesAdditionalInfo= new JSplitPane();
        splitPaneSharesAdditionalInfo.setDividerSize(8);
        spliterPos= splitPaneSharesAdditionalInfo.getWidth() * 0.5;
        pnlShares.setLayout(new FormLayout(new ColumnSpec[]{
                ColumnSpec.decode("487px"),},
                new RowSpec[]{
                        RowSpec.decode("26px"),
                        RowSpec.decode("153px"),
                        RowSpec.decode("250px"),}));

        Box horizontalBoxSharesTreeActions= Box.createHorizontalBox();
        pnlShares.add(horizontalBoxSharesTreeActions, "1, 1, fill, top");

        Component horizontalGlueSharesTreeOptions= Box.createHorizontalGlue();
        horizontalBoxSharesTreeActions.add(horizontalGlueSharesTreeOptions);

        mCmbBxSharesDetailsMode= new JComboBox<>();
        mCmbBxSharesDetailsMode.setEnabled(false);

        SharesDetailsMode sharesDetailsMode[]= new SharesDetailsMode[2];
        sharesDetailsMode[0]= SharesDetailsMode.SHARES;
        sharesDetailsMode[1]= SharesDetailsMode.SHAREES;
        mCmbBxSharesDetailsMode.setModel(new DefaultComboBoxModel<>(sharesDetailsMode));
        horizontalBoxSharesTreeActions.add(mCmbBxSharesDetailsMode);

        Component rigidAreaSharesTreeOptions= Box.createRigidArea(new Dimension(20, 20));
        horizontalBoxSharesTreeActions.add(rigidAreaSharesTreeOptions);

        mChckBxHideSharesTreeFiles= new JCheckBox("Hide Files");
        horizontalBoxSharesTreeActions.add(mChckBxHideSharesTreeFiles);

        mBtnRefreshSharesTreeFiles= new JButton("Refresh");
        horizontalBoxSharesTreeActions.add(mBtnRefreshSharesTreeFiles);
        splitPaneSharesAdditionalInfo.setDividerLocation(225);
        pnlShares.add(splitPaneSharesAdditionalInfo, "1, 2, fill, fill");

        JSplitPane splitPaneSharesMasterDetail= new JSplitPane();
        splitPaneSharesMasterDetail.setDividerSize(8);
        spliterPos= splitPaneSharesAdditionalInfo.getHeight() * 0.5;
        splitPaneSharesMasterDetail.setDividerLocation(70);
        splitPaneSharesMasterDetail.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPaneSharesAdditionalInfo.setRightComponent(splitPaneSharesMasterDetail);

        JScrollPane scrollPaneSharesMaster= new JScrollPane();
        splitPaneSharesMasterDetail.setLeftComponent(scrollPaneSharesMaster);

        mLstSharesMaster= new JList<>();
        mLstSharesMaster.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mLstSharesMaster.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
        mLstSharesMaster.setModel(new DefaultListModel<String>());
        scrollPaneSharesMaster.setViewportView(mLstSharesMaster);

        JScrollPane scrollPaneSharesDetails= new JScrollPane();
        splitPaneSharesMasterDetail.setRightComponent(scrollPaneSharesDetails);

        mLstShareDetail= new JList<>();
        mLstShareDetail.setModel(new DefaultListModel<String>());
        scrollPaneSharesDetails.setViewportView(mLstShareDetail);

        JScrollPane scrollPaneSharesTreeFiles= new JScrollPane();
        splitPaneSharesAdditionalInfo.setLeftComponent(scrollPaneSharesTreeFiles);

        mSharesTreeFiles= new JTree();
        mSharesTreeFiles.setModel(new DefaultTreeModel(null));
        scrollPaneSharesTreeFiles.setViewportView(mSharesTreeFiles);

        JTabbedPane tbdPnSharesActions= new JTabbedPane(JTabbedPane.TOP);
        pnlShares.add(tbdPnSharesActions, "1, 3, fill, top");

        JPanel pnlCreateShare= new JPanel();
        tbdPnSharesActions.addTab("Create", null, pnlCreateShare, null);
        pnlCreateShare.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        mTxtFldSharesFileToBeShared= new JTextField();
        pnlCreateShare.add(mTxtFldSharesFileToBeShared, "2, 2");
        mTxtFldSharesFileToBeShared.setColumns(10);

        mCmbBxSharesCreateSharees= new JComboBox<>();

        mCmbBxSharesCreateSharees.setModel(new DefaultComboBoxModel<OCSharee>());
        pnlCreateShare.add(mCmbBxSharesCreateSharees, "2, 4, fill, default");

        mCmbBxSharesCreateShareType= new JComboBox<>();

        mCmbBxSharesCreateShareType.setModel(new DefaultComboBoxModel<>(ShareType.values()));
        pnlCreateShare.add(mCmbBxSharesCreateShareType, "2, 6, fill, default");

        Box horizontalBoxSharesCreate= Box.createHorizontalBox();
        pnlCreateShare.add(horizontalBoxSharesCreate, "2, 8");

        mChckBxSharesCreateIsPublic= new JCheckBox("Is Public");

        horizontalBoxSharesCreate.add(mChckBxSharesCreateIsPublic);

        mTxtFldSharesCreatePassword= new JTextField();
        mTxtFldSharesCreatePassword.setToolTipText("");
        mTxtFldSharesCreatePassword.setEnabled(false);
        horizontalBoxSharesCreate.add(mTxtFldSharesCreatePassword);
        mTxtFldSharesCreatePassword.setColumns(10);

        mBtnSharesCreateShare= new JButton("Create");
        horizontalBoxSharesCreate.add(mBtnSharesCreateShare);

        JPanel panelSharesCreatePermissions= new JPanel();
        panelSharesCreatePermissions.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        pnlCreateShare.add(panelSharesCreatePermissions, "2, 10, fill, fill");
        panelSharesCreatePermissions.setLayout(new GridLayout(0, 5, 0, 0));

        mChckBxSharesCreatePermissionRead= new JCheckBox("Read");
        panelSharesCreatePermissions.add(mChckBxSharesCreatePermissionRead);

        mChckBxSharesCreatePermissionUpdate= new JCheckBox("Update");
        panelSharesCreatePermissions.add(mChckBxSharesCreatePermissionUpdate);

        mChckBxSharesCreatePermissionCreate= new JCheckBox("Create");
        panelSharesCreatePermissions.add(mChckBxSharesCreatePermissionCreate);

        mChckBxSharesCreatePermissionDelete= new JCheckBox("Delete");
        panelSharesCreatePermissions.add(mChckBxSharesCreatePermissionDelete);

        mChckBxSharesCreatePermissionReShare= new JCheckBox("Re Share");
        panelSharesCreatePermissions.add(mChckBxSharesCreatePermissionReShare);

        JPanel pnlUpdateShare= new JPanel();
        tbdPnSharesActions.addTab("Update", null, pnlUpdateShare, null);
        pnlUpdateShare.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        RowSpec.decode("default:grow"),}));

        mSpinnerSharesRemoteIdToUpdate= new JSpinner();
        mSpinnerSharesRemoteIdToUpdate.setEnabled(false);
        mSpinnerSharesRemoteIdToUpdate.setModel(new SpinnerNumberModel(new Long(0), new Long(0), null, new Long(1)));
        pnlUpdateShare.add(mSpinnerSharesRemoteIdToUpdate, "2, 2");

        mTxtFldSharesPathToUpdate= new JTextField();
        mTxtFldSharesPathToUpdate.setColumns(10);
        pnlUpdateShare.add(mTxtFldSharesPathToUpdate, "2, 4, fill, default");

        mTxtFldSharesUpdateExpirationDate= new JTextField();
        pnlUpdateShare.add(mTxtFldSharesUpdateExpirationDate, "2, 6, fill, default");
        mTxtFldSharesUpdateExpirationDate.setColumns(10);

        Box horizontalBoxSharesUpdate= Box.createHorizontalBox();
        pnlUpdateShare.add(horizontalBoxSharesUpdate, "2, 8");

        mChckBxSharesUpdateIsPublic= new JCheckBox("Is Public");
        horizontalBoxSharesUpdate.add(mChckBxSharesUpdateIsPublic);

        mTxtFldSharesUpdatePassword= new JTextField();
        mTxtFldSharesUpdatePassword.setToolTipText("");
        mTxtFldSharesUpdatePassword.setEnabled(false);
        mTxtFldSharesUpdatePassword.setColumns(10);
        horizontalBoxSharesUpdate.add(mTxtFldSharesUpdatePassword);

        mBtnSharesUpdateShare= new JButton("Update");
        horizontalBoxSharesUpdate.add(mBtnSharesUpdateShare);

        JPanel panelSharesUpdatePermissions= new JPanel();
        panelSharesUpdatePermissions.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        pnlUpdateShare.add(panelSharesUpdatePermissions, "2, 10, fill, fill");
        panelSharesUpdatePermissions.setLayout(new GridLayout(0, 5, 0, 0));

        mChckBxSharesUpdatePermissionRead= new JCheckBox("Read");
        panelSharesUpdatePermissions.add(mChckBxSharesUpdatePermissionRead);

        mChckBxSharesUpdatePermissionUpdate= new JCheckBox("Update");
        panelSharesUpdatePermissions.add(mChckBxSharesUpdatePermissionUpdate);

        mChckBxSharesUpdatePermissionCreate= new JCheckBox("Create");
        panelSharesUpdatePermissions.add(mChckBxSharesUpdatePermissionCreate);

        mChckBxSharesUpdatePermissionDelete= new JCheckBox("Delete");
        panelSharesUpdatePermissions.add(mChckBxSharesUpdatePermissionDelete);

        mChckBxSharesUpdatePermissionReShare= new JCheckBox("Re Share");
        panelSharesUpdatePermissions.add(mChckBxSharesUpdatePermissionReShare);

        JPanel pnlRemoveShare= new JPanel();
        tbdPnSharesActions.addTab("Remove", null, pnlRemoveShare, null);
        pnlRemoveShare.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        JLabel lblSharesRemoteShareIdToRemove= new JLabel("Remote Id");
        pnlRemoveShare.add(lblSharesRemoteShareIdToRemove, "2, 2");

        mSpinnerSharesRemoteShareIdToRemove= new JSpinner();
        mSpinnerSharesRemoteShareIdToRemove.setModel(new SpinnerNumberModel(new Long(0), new Long(0), null, new Long(1)));
        pnlRemoveShare.add(mSpinnerSharesRemoteShareIdToRemove, "2, 4");

        mBtnSharesRemoveShare= new JButton("Remove");

        pnlRemoveShare.add(mBtnSharesRemoveShare, "2, 6");

        JPanel pnlGetShare= new JPanel();
        tbdPnSharesActions.addTab("Share", null, pnlGetShare, null);
        pnlGetShare.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        JLabel lblSharesRemoteId= new JLabel("Remote Id");
        pnlGetShare.add(lblSharesRemoteId, "2, 2");

        mSpinnerSharesRemoteShareIdToGet= new JSpinner();
        mSpinnerSharesRemoteShareIdToGet.setModel(new SpinnerNumberModel(new Long(0), new Long(0), null, new Long(1)));
        pnlGetShare.add(mSpinnerSharesRemoteShareIdToGet, "2, 4");

        mBtnSharesGetShare= new JButton("Get Share");

        pnlGetShare.add(mBtnSharesGetShare, "2, 6");

        JPanel pnlGetSharesForFile= new JPanel();
        tbdPnSharesActions.addTab("Shares for File", null, pnlGetSharesForFile, null);
        pnlGetSharesForFile.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        mTxtFldSharesFileToGetShares= new JTextField();
        mTxtFldSharesFileToGetShares.setEnabled(false);
        pnlGetSharesForFile.add(mTxtFldSharesFileToGetShares, "2, 2, fill, default");
        mTxtFldSharesFileToGetShares.setColumns(10);

        Box horizontalBoxGetSharesForFile= Box.createHorizontalBox();
        pnlGetSharesForFile.add(horizontalBoxGetSharesForFile, "2, 4");

        Component horizontalGlue_1= Box.createHorizontalGlue();
        horizontalBoxGetSharesForFile.add(horizontalGlue_1);

        mChckbGetSharesForFilesReshares= new JCheckBox("Reshares");
        horizontalBoxGetSharesForFile.add(mChckbGetSharesForFilesReshares);

        Component rigidArea_1= Box.createRigidArea(new Dimension(20, 20));
        horizontalBoxGetSharesForFile.add(rigidArea_1);

        mChckbGetSharesForFilesSubfiles= new JCheckBox("Subfiles");
        horizontalBoxGetSharesForFile.add(mChckbGetSharesForFilesSubfiles);

        mBtnGetSharesForFile= new JButton("Get Shares");

        pnlGetSharesForFile.add(mBtnGetSharesForFile, "2, 6");

        JPanel pnlGetShares= new JPanel();
        tbdPnSharesActions.addTab("Shares", null, pnlGetShares, null);
        pnlGetShares.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        mBtnListShares= new JButton("Shares");
        pnlGetShares.add(mBtnListShares, "2, 2");

        JPanel pnlGetSharees= new JPanel();
        tbdPnSharesActions.addTab("Sharees", null, pnlGetSharees, null);
        pnlGetSharees.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        mTxtFldShareesSearchString= new JTextField();
        pnlGetSharees.add(mTxtFldShareesSearchString, "2, 2, fill, center");
        mTxtFldShareesSearchString.setColumns(10);

        Box horizontalBoxListShareesPaging= Box.createHorizontalBox();
        pnlGetSharees.add(horizontalBoxListShareesPaging, "2, 4");

        JLabel lblShareesPageNumber= new JLabel("Page Number");
        horizontalBoxListShareesPaging.add(lblShareesPageNumber);

        mSpnrShareesPageNumber= new JSpinner();
        mSpnrShareesPageNumber.setModel(new SpinnerNumberModel(new Integer(1), null, null, new Integer(1)));
        horizontalBoxListShareesPaging.add(mSpnrShareesPageNumber);

        Component horizontalGlueListShareesPaging= Box.createHorizontalGlue();
        horizontalBoxListShareesPaging.add(horizontalGlueListShareesPaging);

        JLabel lblShareesItensPerPage= new JLabel("Itens per Page");
        horizontalBoxListShareesPaging.add(lblShareesItensPerPage);

        mSpnrShareesItensPerPage= new JSpinner();
        mSpnrShareesItensPerPage.setModel(new SpinnerNumberModel(new Integer(25), null, null, new Integer(1)));
        horizontalBoxListShareesPaging.add(mSpnrShareesItensPerPage);

        mBtnListSharees= new JButton("List Sharees");

        pnlGetSharees.add(mBtnListSharees, "2, 6");

        JPanel pnlStatus= new JPanel();
        mTbdPnActions.addTab("Status", null, pnlStatus, null);
        pnlStatus.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPaneServerInfo= new JScrollPane();
        pnlStatus.add(scrollPaneServerInfo);

        mLstServerInfo= new JList<>();
        scrollPaneServerInfo.setViewportView(mLstServerInfo);
        mLstServerInfo.setModel(new DefaultListModel<String>());

        JTabbedPane tbdPnStatusActions= new JTabbedPane(JTabbedPane.TOP);
        pnlStatus.add(tbdPnStatusActions, BorderLayout.SOUTH);

        JPanel pnlStatusActions= new JPanel();
        tbdPnStatusActions.addTab("Actions", null, pnlStatusActions, null);
        pnlStatusActions.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,}));

        mBtnGetServerStatus= new JButton("Get Status");
        pnlStatusActions.add(mBtnGetServerStatus, "2, 2");

        mBtnGetServerCapabilities= new JButton("Get Capabilities");
        pnlStatusActions.add(mBtnGetServerCapabilities, "2, 4");

        JPanel pnlUsers= new JPanel();
        mTbdPnActions.addTab("Users", null, pnlUsers, null);
        pnlUsers.setLayout(new BorderLayout(0, 0));

        mLblUserAvatar= new JLabel("Avatar");
        pnlUsers.add(mLblUserAvatar, BorderLayout.NORTH);

        mLstUserData= new JList<>();
        pnlUsers.add(mLstUserData, BorderLayout.CENTER);
        mLstUserData.setModel(new DefaultListModel<String>());

        JTabbedPane tbdPnUsersActions= new JTabbedPane(JTabbedPane.TOP);
        pnlUsers.add(tbdPnUsersActions, BorderLayout.SOUTH);

        JPanel pnlUserActions= new JPanel();
        tbdPnUsersActions.addTab("Actions", null, pnlUserActions, null);
        pnlUserActions.setLayout(new FormLayout(new ColumnSpec[]{
                FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormSpecs.RELATED_GAP_COLSPEC,},
                new RowSpec[]{
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC,}));

        mBtnGetUserInfo= new JButton("Get Info");
        pnlUserActions.add(mBtnGetUserInfo, "2, 2");

        mBtnGetUserAvatar= new JButton("Get Avatar");
        pnlUserActions.add(mBtnGetUserAvatar, "2, 4");

        mBtnGetUserQuota= new JButton("Get Quota");
        pnlUserActions.add(mBtnGetUserQuota, "2, 6");

        JScrollPane scrollPaneLog= new JScrollPane();
        splitPane.setRightComponent(scrollPaneLog);

        mLstLog= new JList<>();
        mLstLog.setModel(new DefaultListModel<String>());
        scrollPaneLog.setViewportView(mLstLog);

        JPopupMenu popupMenuLog= new JPopupMenu();
        JMenuItem menuItem= new JMenuItem("Clear");
        menuItem.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearLog();
            }
        });

        popupMenuLog.add(menuItem);
        addPopup(mLstLog, popupMenuLog);

    }

    private void initializeUIData(){
        mBtnListSharees.doClick();
        refreshFilesTree();
        refreshSharesTree();
    }

    private void initializeBindEvents(){
        mCmbBxLoginMethod.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e){
                if(e.getStateChange() == ItemEvent.SELECTED)
                    validateLoginMethod((LoginMethod) e.getItem());
            }
        });

        mBtnApplyCredentials.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0){
                applyCredentials((LoginMethod) mCmbBxLoginMethod.getSelectedItem());
                initializeUIData();
            }
        });

        mBtnRevokeCredentials.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0){
                revokeCredentials();
            }
        });

        mBtnSharesCreateShare.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearSharesMaster();
                String remoteFilePath= mTxtFldSharesFileToBeShared.getText();
                ShareType shareType= (ShareType) mCmbBxSharesCreateShareType.getSelectedItem();
                String shareWith= ((OCSharee) mCmbBxSharesCreateSharees.getSelectedItem()).getShareWith();
                boolean publicUpload= mChckBxSharesCreateIsPublic.isSelected();
                String password= mTxtFldSharesCreatePassword.getText();
                int permissions= getSelectedSharesCreatePermissions();

                CreateRemoteShareOperation operation= new CreateRemoteShareOperation(remoteFilePath, shareType, shareWith, publicUpload, password, permissions);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleCreateRemoteShareOperationResult(result);
                }
            }
        });

        mCmbBxSharesCreateShareType.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                ShareType shareType= (ShareType) mCmbBxSharesCreateShareType.getSelectedItem();
                switch(shareType){
                    case NO_SHARED:
                        break;
                    case USER:
                        break;
                    case GROUP:
                        break;
                    case PUBLIC_LINK:
                        break;
                    case EMAIL:
                        break;
                    case CONTACT:
                        break;
                    case FEDERATED:
                        break;
                    default:
                        break;

                }
                mCmbBxSharesCreateSharees.setEnabled(ShareType.USER.equals(shareType) || ShareType.GROUP.equals(shareType));
            }
        });

        mChckBxSharesCreateIsPublic.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mTxtFldSharesCreatePassword.setEnabled(!mChckBxSharesCreateIsPublic.isSelected());
                if(mChckBxUseAsynchronousAPICalls.isSelected()){

                }else{

                }
            }
        });

        mBtnSharesUpdateShare.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                try{
                    mSpinnerSharesRemoteIdToUpdate.commitEdit();
                }catch(ParseException e1){
                    e1.printStackTrace();
                }
                Long remoteId= (Long) mSpinnerSharesRemoteIdToUpdate.getValue();
                boolean publicUpload= mChckBxSharesUpdateIsPublic.isSelected();
                String password= mTxtFldSharesUpdatePassword.getText();
                int permissions= getSelectedSharesUpdatePermissions();

                UpdateRemoteShareOperation operation= new UpdateRemoteShareOperation(remoteId);
                operation.setPublicUpload(publicUpload);
                if(!publicUpload && !password.isEmpty())
                    operation.setPassword(password);
                operation.setPermissions(permissions);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleUpdateRemoteShareOperationResult(result);
                }
            }
        });

        mChckBxSharesUpdateIsPublic.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mTxtFldSharesUpdatePassword.setEnabled(!mChckBxSharesUpdateIsPublic.isSelected());
                if(mChckBxUseAsynchronousAPICalls.isSelected()){

                }else{

                }
            }
        });

        mBtnGetServerCapabilities.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearServerInfo();
                GetRemoteCapabilitiesOperation operarion= new GetRemoteCapabilitiesOperation();
                mTxtFldSharesCreatePassword.setEnabled(mChckBxSharesCreateIsPublic.isSelected());
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operarion.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operarion.execute(mClient);
                    handleGetRemoteCapabilitiesOperationResult(result);
                }
            }
        });

        mBtnGetServerStatus.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearServerInfo();
                GetRemoteStatusOperation getRemoteStatusOperation= new GetRemoteStatusOperation();
                mTxtFldSharesCreatePassword.setEnabled(mChckBxSharesCreateIsPublic.isSelected());
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    getRemoteStatusOperation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= getRemoteStatusOperation.execute(mClient);
                    handleGetRemoteStatusOperationResult(result);
                }
            }
        });

        mBtnGetUserQuota.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearUserData();
                GetRemoteUserQuotaOperation getUserQuotaOperation= new GetRemoteUserQuotaOperation();
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    getUserQuotaOperation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= getUserQuotaOperation.execute(mClient);
                    handleGetRemoteUserQuotaOperationResult(result);
                }
            }
        });

        mBtnGetUserAvatar.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearUserAvatar();
                GetRemoteUserAvatarOperation getUserAvatarOperation= new GetRemoteUserAvatarOperation(64);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    getUserAvatarOperation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= getUserAvatarOperation.execute(mClient);
                    handleGetRemoteUserAvatarOperationResult(result);
                }
            }
        });

        mBtnGetUserInfo.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                clearUserData();
                GetRemoteUserInfoOperation getUserInfoOperation= new GetRemoteUserInfoOperation();
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    getUserInfoOperation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= getUserInfoOperation.execute(mClient);
                    handleGetRemoteUserInfoOperationResult(result);
                }
            }
        });

        mChckBxUseAsynchronousAPICalls.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    log("App using asynchronous API calls");
                else
                    log("App using synchronous API calls");
            }
        });

        mTxtFldSyncDirectory.addFocusListener(new FocusListener(){

            @Override
            public void focusLost(FocusEvent e){
                File folderToCheck= new File(mTxtFldSyncDirectory.getText());
                if(!folderToCheck.exists()){
                    String folderName= folderToCheck.getName();
                    int selectedOption= JOptionPane.showConfirmDialog(null, "The folder '" + folderName + "' does not exist. Create it?", "Create Folder", JOptionPane.YES_NO_OPTION);
                    if(selectedOption == JOptionPane.YES_OPTION){
                        folderToCheck.mkdirs();
                        log("Folder created :" + folderToCheck.getName());
                    }
                }
            }

            @Override
            public void focusGained(FocusEvent e){
                // Do nothing
            }
        });

        mBtnSelectSyncDirectory.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mDirectoryChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                    log("Sync directory chooser dialog returned : " + mDirectoryChooser.getCurrentDirectory());
                    mTxtFldSyncDirectory.setText(mDirectoryChooser.getCurrentDirectory().toString());
                }else
                    log("Sync directory chooser dialog returned no Selection");
            }
        });

        mBtnRefreshFilesTreeFiles.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    refreshFilesTree();
                else
                    refreshFilesTree();

            }
        });

        mBtnRefreshSharesTreeFiles.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    refreshSharesTree();
                else
                    refreshSharesTree();

            }
        });

        mBtnCreateDirectory.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                TreePath treePath= mFilesTreeFiles.getSelectionPath();

                String webDavPath= treePathToWebDAVPath(treePath);
                webDavPath= webDavPath.endsWith(FileUtils.PATH_SEPARATOR) ? webDavPath : webDavPath + FileUtils.PATH_SEPARATOR;
                webDavPath+= mTxtFldNewDirectoryName.getText();
                webDavPath= webDavPath.endsWith(FileUtils.PATH_SEPARATOR) ? webDavPath : webDavPath + FileUtils.PATH_SEPARATOR;
                CreateRemoteFolderOperation operation= new CreateRemoteFolderOperation(webDavPath, mChckBxCreateFullPath.isSelected());
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    executeRemoteOperation(operation, TestApplication.this);
                else{
                    executeRemoteOperation(operation);
                    refreshFilesTree();
                }
            }
        });

        mFilesTreeFiles.addTreeWillExpandListener(new TreeWillExpandListener(){

            @Override
            public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException{
                TreePath path= event.getPath();
                Path treePath= treePathToPath(event.getPath());
                String webDAVTreePath= treePath.toString().replace("\\", FileUtils.PATH_SEPARATOR);

                DefaultMutableTreeNode node= (DefaultMutableTreeNode) path.getLastPathComponent();
                if(mFilesTreeLoadedNodes.get(treePath.toString()) == null || mFilesTreeLoadedNodes.get(treePath.toString()) == false){
                    ReadRemoteFolderOperation operation= new ReadRemoteFolderOperation(webDAVTreePath);

                    RemoteOperationResult ror= executeRemoteOperation(operation);
                    List<RemoteFile> filesAndFolders= toRemoteFiles(ror.getData());
                    // Remove the root element
                    filesAndFolders.remove(0);
                    buildTreeModel((DefaultTreeModel) mFilesTreeFiles.getModel(), node, filesAndFolders, mFilesTreeLoadedNodes, mFilesTreeElements);
                    mFilesTreeFiles.expandRow(0);

                    mFilesTreeLoadedNodes.put(treePath.toString(), true);
                }

            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException{
                // Do nothing
            }
        });

        mSharesTreeFiles.addTreeWillExpandListener(new TreeWillExpandListener(){

            @Override
            public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException{
                TreePath path= event.getPath();
                Path treePath= treePathToPath(event.getPath());
                String webDAVTreePath= treePath.toString().replace("\\", FileUtils.PATH_SEPARATOR);

                DefaultMutableTreeNode node= (DefaultMutableTreeNode) path.getLastPathComponent();
                if(mSharesTreeLoadedNodes.get(treePath.toString()) == null || mSharesTreeLoadedNodes.get(treePath.toString()) == false){
                    ReadRemoteFolderOperation operation= new ReadRemoteFolderOperation(webDAVTreePath);

                    RemoteOperationResult ror= executeRemoteOperation(operation);
                    List<RemoteFile> filesAndFolders= toRemoteFiles(ror.getData());
                    filesAndFolders.remove(0);
                    buildTreeModel((DefaultTreeModel) mSharesTreeFiles.getModel(), node, filesAndFolders, mSharesTreeLoadedNodes, mSharesTreeElements);
                    mSharesTreeFiles.expandRow(0);

                    mSharesTreeLoadedNodes.put(treePath.toString(), true);
                }

            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException{
                // Do nothing
            }
        });

        mChckBxHideFilesTreeFiles.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mChckBxHideFilesTreeFiles.isSelected())
                    mFilesTreeElements.remove(OCTreeElements.FILE);
                else
                    mFilesTreeElements.add(OCTreeElements.FILE);
                refreshFilesTree();
            }
        });

        mChckBxHideSharesTreeFiles.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mChckBxHideSharesTreeFiles.isSelected())
                    mSharesTreeElements.remove(OCTreeElements.FILE);
                else
                    mSharesTreeElements.add(OCTreeElements.FILE);
                refreshSharesTree();
            }
        });

        mFilesTreeFiles.addTreeSelectionListener(new TreeSelectionListener(){

            @Override
            public void valueChanged(TreeSelectionEvent e){
                TreePath treePath= e.getPath();
                DefaultMutableTreeNode node= (DefaultMutableTreeNode) treePath.getLastPathComponent();
                Boolean isFile= node.isLeaf() && !node.getAllowsChildren();
                Boolean isDirectory= node.getAllowsChildren();

                Path path= treePathToPath(treePath);
                String webDAVTreePath= path.toString().replace("\\", FileUtils.PATH_SEPARATOR);

                mBtnDownloadFile.setEnabled(isFile);
                mBtnCreateDirectory.setEnabled(isDirectory);
                if(isFile){
                    mTxtFldRemoteFilePathToDownload.setText(webDAVTreePath);
                    mTxtFldFileOrDirToBeRenamed.setText(webDAVTreePath);
                    mTxtFldFileOrDirNewName.setText(treePath.getLastPathComponent().toString());
                    mTxtFldExistenceCheck.setText(webDAVTreePath);
                }

                if(isDirectory){
                    webDAVTreePath+= !webDAVTreePath.endsWith(FileUtils.PATH_SEPARATOR) ? FileUtils.PATH_SEPARATOR : "";

                    mTxtFldFileOrDirToBeRenamed.setText(webDAVTreePath);
                    mTxtFldFileOrDirNewName.setText(treePath.getLastPathComponent().toString());
                    mTxtFldExistenceCheck.setText(webDAVTreePath);
                }

                if(mChckBxGetCopySourceFromTree.isSelected())
                    mTxtFldSourceFileOrDirForCopy.setText(webDAVTreePath);
                if(mChckBxGetCopyDestinyFromTree.isSelected())
                    mTxtFldDestinyPathForCopy.setText(webDAVTreePath);
            }
        });

        mSharesTreeFiles.addTreeSelectionListener(new TreeSelectionListener(){

            @Override
            public void valueChanged(TreeSelectionEvent e){
                TreePath treePath= e.getPath();
                DefaultMutableTreeNode node= (DefaultMutableTreeNode) treePath.getLastPathComponent();
                Boolean isRoot= treePath.getParentPath() == null;
                Boolean isFile= node.isLeaf() && !node.getAllowsChildren();
                Boolean isDirectory= node.getAllowsChildren();

                Path path= treePathToPath(treePath);
                String webDAVTreePath= path.toString().replace("\\", FileUtils.PATH_SEPARATOR);

                mChckbGetSharesForFilesSubfiles.setEnabled(isDirectory);
                mBtnSharesCreateShare.setEnabled(!isRoot);

                if(isFile){
                    mTxtFldSharesFileToGetShares.setText(webDAVTreePath);
                    mTxtFldSharesFileToBeShared.setText(webDAVTreePath);
                    mTxtFldSharesPathToUpdate.setText(webDAVTreePath);
                }

                if(isDirectory){
                    webDAVTreePath+= !webDAVTreePath.endsWith(FileUtils.PATH_SEPARATOR) ? FileUtils.PATH_SEPARATOR : "";
                    mTxtFldSharesFileToGetShares.setText(webDAVTreePath);
                    mTxtFldSharesFileToBeShared.setText(webDAVTreePath);
                    mTxtFldSharesPathToUpdate.setText(webDAVTreePath);
                }

                if(!isRoot)
                    mBtnGetSharesForFile.doClick();
            }
        });

        mBtnRemoveFileOrDir.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                int response= JOptionPane.showConfirmDialog(null, "Delete the selected iten(s)?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(response == JOptionPane.YES_OPTION){
                    TreePath[] paths= mFilesTreeFiles.getSelectionPaths();
                    for(TreePath treePath: paths){
                        Path path= treePathToPath(treePath);
                        String webDAVTreePath= path.toString().replace("\\", FileUtils.PATH_SEPARATOR);
                        RemoveRemoteFileOperation operation= new RemoveRemoteFileOperation(webDAVTreePath);
                        if(mChckBxUseAsynchronousAPICalls.isSelected())
                            executeRemoteOperation(operation, TestApplication.this);
                        else{
                            executeRemoteOperation(operation);
                            refreshFilesTree();
                        }
                    }
                }
            }
        });

        mBtnSelectFileToUpload.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mFileToUploadChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                    log("File to upload chooser dialog returned : " + mFileToUploadChooser.getSelectedFile());
                    mTxtFldUploadFile.setText(mFileToUploadChooser.getSelectedFile().toString());
                }else
                    log("File to upload chooser dialog returned no Selection");
            }
        });

        mBtnUploadFile.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                String localFilePath= mTxtFldUploadFile.getText();
                File fileToUpload= new File(localFilePath);
                if(fileToUpload.exists() && fileToUpload.isFile()){
                    // Local file
                    String localFilelToUpload= fileToUpload.getAbsolutePath();

                    // Remote Directory where the file will be uploaded
                    TreePath treePathToUpload= mFilesTreeFiles.getSelectionPath();
                    DefaultMutableTreeNode node= null;
                    if(treePathToUpload == null){
                        log("No folder selected in tree to upload file. Will upload to root.");
                        node= (DefaultMutableTreeNode) mFilesTreeFiles.getModel().getRoot();
                        treePathToUpload= new TreePath(node.getPath());
                    }else
                        node= (DefaultMutableTreeNode) treePathToUpload.getLastPathComponent();
                    // Verify if the node represents a File
                    Path pathToUpload= null;
                    if(node.isLeaf() && !node.getAllowsChildren()){
                        // If so, we will use its parent directory
                        node= (DefaultMutableTreeNode) node.getParent();
                        pathToUpload= treePathToPath(new TreePath(node.getPath()));
                    }else
                        pathToUpload= treePathToPath(treePathToUpload);
                    String webDAVTreePathToUpload= pathToUpload.toString().replace("\\", FileUtils.PATH_SEPARATOR);
                    webDAVTreePathToUpload+= FileUtils.PATH_SEPARATOR;
                    webDAVTreePathToUpload+= fileToUpload.getName();

                    // Mime type
                    MimetypesFileTypeMap mimetypesFileTypeMap= new MimetypesFileTypeMap();
                    String mimeType= mimetypesFileTypeMap.getContentType(fileToUpload);

                    UploadRemoteFileOperation operation= new UploadRemoteFileOperation(localFilelToUpload, webDAVTreePathToUpload, mimeType);
                    if(mChckBxUseAsynchronousAPICalls.isSelected())
                        executeRemoteOperation(operation, TestApplication.this);
                    else{
                        RemoteOperationResult result= executeRemoteOperation(operation);
                        handleUploadRemoteFileOperation(result);
                        refreshFilesTree();
                    }
                }else
                    log("Selected item does not exis or is not a file: " + fileToUpload.getAbsolutePath());
            }
        });

        mBtnDownloadFile.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                String webDavPath= mTxtFldRemoteFilePathToDownload.getText();
                webDavPath= webDavPath.endsWith(FileUtils.PATH_SEPARATOR) ? webDavPath : webDavPath + FileUtils.PATH_SEPARATOR;
                DownloadRemoteFileOperation operation= new DownloadRemoteFileOperation(webDavPath, mTxtFldSyncDirectory.getText());
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    executeRemoteOperation(operation, TestApplication.this);
                else{
                    RemoteOperationResult result= executeRemoteOperation(operation);
                    handleDownloadRemoteFileOperation(result);
                }
            }
        });

        mTbdPnActions.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e){
                // Indexes
                // 0 : Files
                // 1 : Shares
                // 2 : Status
                // 3 : Users
                switch(mTbdPnActions.getSelectedIndex()){
                    case 0:{
                        if(mFilesTreeFiles.getModel().getRoot() == null)
                            refreshFilesTree();
                    }
                        break;
                    case 1:{
                        if(mSharesTreeFiles.getModel().getRoot() == null)
                            refreshSharesTree();
                    }
                        break;
                }
            }
        });

        mBtnRenameFileOrDir.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                TreePath treePath= mFilesTreeFiles.getSelectionPath();
                // DefaultMutableTreeNode node= (DefaultMutableTreeNode)
                // treePath.getLastPathComponent();
                // Boolean isFile= (node.isLeaf() && !node.getAllowsChildren());
                // Boolean isDirectory= node.getAllowsChildren();

                String oldName= treePath.getLastPathComponent().toString();
                String oldRemotePath= treePathToWebDAVPath(treePath).toString();
                String newName= mTxtFldFileOrDirNewName.getText();

                RenameRemoteFileOperation operation= new RenameRemoteFileOperation(oldName, oldRemotePath, newName, false);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    executeRemoteOperation(operation, TestApplication.this);
                else{
                    RemoteOperationResult result= executeRemoteOperation(operation);
                    handleRenameRemoteFileOperation(result);
                }
            }

        });

        mBtnExistenceCheck.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                String pathToCheck= mTxtFldExistenceCheck.getText();
                ExistenceCheckRemoteOperation operation= new ExistenceCheckRemoteOperation(pathToCheck, mChckBxSuccessIfAbsent.isSelected());
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    executeRemoteOperation(operation, TestApplication.this);
                else{
                    RemoteOperationResult result= executeRemoteOperation(operation);
                    handleExistenceCheckRemoteOperation(result);
                }
            }
        });

        mBtnReadInfo.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                TreePath treePath= mFilesTreeFiles.getSelectionPath();
                DefaultMutableTreeNode node= (DefaultMutableTreeNode) treePath.getLastPathComponent();
                // Boolean isFile= (node.isLeaf() && !node.getAllowsChildren());
                Boolean isDirectory= node.getAllowsChildren();

                String remotePath= treePathToWebDAVPath(treePath);
                if(isDirectory && !remotePath.endsWith(FileUtils.PATH_SEPARATOR))
                    remotePath+= FileUtils.PATH_SEPARATOR;

                ReadRemoteFileOperation operation= new ReadRemoteFileOperation(remotePath);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    executeRemoteOperation(operation, TestApplication.this);
                else{
                    RemoteOperationResult result= executeRemoteOperation(operation);
                    handleReadRemoteFileOperation(result);
                }
            }
        });

        mBtnCopyFileOrDir.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                String sourceFileOrDir= mTxtFldSourceFileOrDirForCopy.getText();
                String destinyPath= mTxtFldDestinyPathForCopy.getText();
                Boolean isDestinyPathAFolder= destinyPath.endsWith(FileUtils.PATH_SEPARATOR);

                Boolean isRoot= sourceFileOrDir.equals(FileUtils.PATH_SEPARATOR);
                if(!isRoot && !destinyPath.isEmpty() && isDestinyPathAFolder){
                    Boolean isDirectory= sourceFileOrDir.endsWith(FileUtils.PATH_SEPARATOR);

                    String srcRemotePath= sourceFileOrDir;
                    File file= new File(srcRemotePath);
                    String targetRemotePath= destinyPath + file.getName();
                    CopyRemoteFileOperation operation= new CopyRemoteFileOperation(srcRemotePath, targetRemotePath, isDirectory);
                    if(mChckBxUseAsynchronousAPICalls.isSelected())
                        executeRemoteOperation(operation, TestApplication.this);
                    else{
                        RemoteOperationResult result= executeRemoteOperation(operation);
                        handleCopyRemoteFileOperation(result);
                    }
                }else{
                    if(isRoot)
                        log("Invalid source element: Root is not allowed");
                    if(destinyPath.isEmpty())
                        log("Destiny path is empty");
                    if(!isDestinyPathAFolder)
                        log("Destiny path must be a folder");
                }
            }
        });

        mBtnMoveFileOrDir.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                String sourceFileOrDir= mTxtFldSourceFileOrDirForMove.getText();
                String destinyPath= mTxtFldDestinyPathForMove.getText();
                Boolean isDestinyPathAFolder= destinyPath.endsWith(FileUtils.PATH_SEPARATOR);

                Boolean isRoot= sourceFileOrDir.equals(FileUtils.PATH_SEPARATOR);
                if(!isRoot && !destinyPath.isEmpty() && isDestinyPathAFolder){
                    Boolean isDirectory= sourceFileOrDir.endsWith(FileUtils.PATH_SEPARATOR);

                    String srcRemotePath= sourceFileOrDir;
                    File file= new File(srcRemotePath);
                    String targetRemotePath= destinyPath + file.getName();
                    MoveRemoteFileOperation operation= new MoveRemoteFileOperation(srcRemotePath, targetRemotePath, isDirectory);
                    if(mChckBxUseAsynchronousAPICalls.isSelected())
                        executeRemoteOperation(operation, TestApplication.this);
                    else{
                        RemoteOperationResult result= executeRemoteOperation(operation);
                        handleMoveRemoteFileOperation(result);
                    }
                }else{
                    if(isRoot)
                        log("Invalid source element: Root is not allowed");
                    if(destinyPath.isEmpty())
                        log("Destiny path is empty");
                    if(!isDestinyPathAFolder)
                        log("Destiny path must be a folder");
                }
            }
        });

        mBtnSelectFileToChunkUpload.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(mFileToUploadChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                    log("File to upload chooser dialog returned : " + mFileToUploadChooser.getSelectedFile());
                    mTxtFldChunkedUploadFile.setText(mFileToUploadChooser.getSelectedFile().toString());
                }else
                    log("File to upload chooser dialog returned no Selection");
            }
        });

        mBtnChunkedUploadFile.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                String localFilePath= mTxtFldChunkedUploadFile.getText();
                File fileToUpload= new File(localFilePath);
                if(fileToUpload.exists() && fileToUpload.isFile()){
                    // Local file
                    String localFilelToUpload= fileToUpload.getAbsolutePath();

                    // Remote Directory where the file will be uploaded
                    TreePath treePathToUpload= mFilesTreeFiles.getSelectionPath();
                    DefaultMutableTreeNode node= null;
                    if(treePathToUpload == null){
                        log("No folder selected in tree to upload file. Will upload to root.");
                        node= (DefaultMutableTreeNode) mFilesTreeFiles.getModel().getRoot();
                        treePathToUpload= new TreePath(node.getPath());
                    }else
                        node= (DefaultMutableTreeNode) treePathToUpload.getLastPathComponent();
                    // Verify if the node represents a File
                    Path pathToUpload= null;
                    if(node.isLeaf() && !node.getAllowsChildren()){
                        // If so, we will use its parent directory
                        node= (DefaultMutableTreeNode) node.getParent();
                        pathToUpload= treePathToPath(new TreePath(node.getPath()));
                    }else
                        pathToUpload= treePathToPath(treePathToUpload);
                    String webDAVTreePathToUpload= pathToUpload.toString().replace("\\", FileUtils.PATH_SEPARATOR);
                    webDAVTreePathToUpload+= FileUtils.PATH_SEPARATOR;
                    webDAVTreePathToUpload+= fileToUpload.getName();

                    // Mime type
                    MimetypesFileTypeMap mimetypesFileTypeMap= new MimetypesFileTypeMap();
                    String mimeType= mimetypesFileTypeMap.getContentType(fileToUpload);

                    ChunkedUploadRemoteFileOperation operation= new ChunkedUploadRemoteFileOperation(localFilelToUpload, webDAVTreePathToUpload, mimeType);
                    if(mChckBxUseAsynchronousAPICalls.isSelected())
                        executeRemoteOperation(operation, TestApplication.this);
                    else{
                        RemoteOperationResult result= executeRemoteOperation(operation);
                        handleUploadRemoteFileOperation(result);
                        refreshFilesTree();
                    }
                }else
                    log("Selected item does not exis or is not a file: " + fileToUpload.getAbsolutePath());
            }
        });

        mBtnListSharees.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mCmbBxSharesDetailsMode.setSelectedItem(SharesDetailsMode.SHAREES);
                String query= mTxtFldShareesSearchString.getText();
                try{
                    mSpnrShareesPageNumber.commitEdit();
                }catch(ParseException pex){}
                int page= (Integer) mSpnrShareesPageNumber.getValue();

                try{
                    mSpnrShareesItensPerPage.commitEdit();
                }catch(ParseException pex){}
                int perPage= (Integer) mSpnrShareesItensPerPage.getValue();

                GetRemoteShareesOperation operation= new GetRemoteShareesOperation(query, page, perPage);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleGetRemoteShareesOperationResult(result);
                }
            }
        });

        mLstSharesMaster.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e){
                if(!e.getValueIsAdjusting()){
                    clearSharesDetail();
                    clearSharesUpdateShareData();
                    if(mLstSharesMaster.getSelectedIndex() > -1){
                        SharesDetailsMode sharesDetailsMode= (SharesDetailsMode) mCmbBxSharesDetailsMode.getSelectedItem();
                        switch(sharesDetailsMode){
                            case SHAREES:
                                JSONObject jsonObject= (JSONObject) mSharesMasterObjects.get(mLstSharesMaster.getSelectedIndex());
                                showSharesDetail(jsonObject);
                                break;
                            case SHARES:
                                OCShare ocShare= (OCShare) mSharesMasterObjects.get(mLstSharesMaster.getSelectedIndex());
                                mSpinnerSharesRemoteShareIdToRemove.setValue(ocShare.getRemoteId());
                                mSpinnerSharesRemoteShareIdToGet.setValue(ocShare.getRemoteId());
                                showSharesDetail(ocShare);
                                populateUpdateShareData(ocShare);
                                break;
                            case UNKNOWN:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        });

        mBtnListShares.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mCmbBxSharesDetailsMode.setSelectedItem(SharesDetailsMode.SHARES);
                GetRemoteSharesOperation operation= new GetRemoteSharesOperation();
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleGetRemoteSharesOperationResult(result);
                }
            }
        });

        mCmbBxSharesDetailsMode.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e){
                if(e.getStateChange() == ItemEvent.SELECTED){
                    mSharesMasterObjects.clear();
                    clearSharesMaster();
                    clearSharesDetail();
                }
            }
        });

        mBtnGetSharesForFile.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mCmbBxSharesDetailsMode.setSelectedItem(SharesDetailsMode.SHARES);
                clearSharesMaster();
                String remoteFilePath= mTxtFldSharesFileToGetShares.getText();
                Boolean reshares= mChckbGetSharesForFilesReshares.isSelected();
                Boolean subfiles= mChckbGetSharesForFilesSubfiles.isEnabled() ? mChckbGetSharesForFilesSubfiles.isSelected() : false;
                GetRemoteSharesForFileOperation operation= new GetRemoteSharesForFileOperation(remoteFilePath, reshares, subfiles);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleGetRemoteSharesForFileOperationResult(result);
                }
            }
        });

        mBtnSharesGetShare.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mCmbBxSharesDetailsMode.setSelectedItem(SharesDetailsMode.SHARES);
                clearSharesMaster();
                try{
                    mSpinnerSharesRemoteShareIdToGet.commitEdit();
                }catch(ParseException e1){
                    e1.printStackTrace();
                }
                Long remoteId= (Long) mSpinnerSharesRemoteShareIdToGet.getValue();
                GetRemoteShareOperation operation= new GetRemoteShareOperation(remoteId);
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleGetRemoteShareOperationResult(result);
                }
            }
        });

        mBtnSharesRemoveShare.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                mCmbBxSharesDetailsMode.setSelectedItem(SharesDetailsMode.SHARES);
                clearSharesMaster();
                try{
                    mSpinnerSharesRemoteShareIdToRemove.commitEdit();
                }catch(ParseException e1){
                    e1.printStackTrace();
                }
                Long remoteShareId= (Long) mSpinnerSharesRemoteShareIdToRemove.getValue();
                RemoveRemoteShareOperation operation= new RemoveRemoteShareOperation(remoteShareId.intValue());
                if(mChckBxUseAsynchronousAPICalls.isSelected())
                    operation.execute(mClient, TestApplication.this, mExecutor);
                else{
                    RemoteOperationResult result= operation.execute(mClient);
                    handleRemoveRemoteShareOperationResult(result);
                }
            }
        });

        mCmbBxSharesCreateSharees.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e){
                if(e.getStateChange() == ItemEvent.SELECTED){
                    OCSharee sharee= (OCSharee) mCmbBxSharesCreateSharees.getSelectedItem();
                    mCmbBxSharesCreateShareType.setSelectedItem(sharee.getShareType());
                }
            }
        });

    }

    private void validateLoginMethod(LoginMethod method){
        switch(method){
            case DEFAULT:
            case ANONYMOUS:
                mBtnApplyCredentials.setEnabled(true);
                break;
            case SAML_SSO:
            case TOKEN:
                mBtnApplyCredentials.setEnabled(false);
                log("Login method not supported: " + method.toString());
                break;
            case UNKNOWN:
            default:
                mBtnApplyCredentials.setEnabled(false);
                log("No login method selected");
                break;
            //
        }
    }

    // Interfaces Implementations
    /////////////////////////////

    @Override
    public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result){
        // Files & Directories
        if(caller instanceof ChunkedUploadRemoteFileOperation){
            log("Remote Operation ChunkedUploadRemoteFileOperation finished");
            handleChunkedUploadRemoteFileOperation(result);
        }else if(caller instanceof CopyRemoteFileOperation){
            log("Remote Operation CopyRemoteFileOperation finished");
            handleCopyRemoteFileOperation(result);
        }else if(caller instanceof CreateRemoteFolderOperation){
            log("Remote Operation CreateRemoteFolderOperation finished");
            handleCreateRemoteFolderOperation(result);
        }else if(caller instanceof DownloadRemoteFileOperation){
            log("Remote Operation DownloadRemoteFileOperation finished");
            handleDownloadRemoteFileOperation(result);
        }else if(caller instanceof ExistenceCheckRemoteOperation){
            log("Remote Operation ExistenceCheckRemoteOperation finished");
            handleExistenceCheckRemoteOperation(result);
        }else if(caller instanceof MoveRemoteFileOperation){
            log("Remote Operation MoveRemoteFileOperation finished");
            handleMoveRemoteFileOperation(result);
        }else if(caller instanceof ReadRemoteFileOperation){
            log("Remote Operation ReadRemoteFileOperation finished");
            handleReadRemoteFileOperation(result);
        }else if(caller instanceof ReadRemoteFolderOperation){
            log("Remote Operation ReadRemoteFolderOperation finished");
            handleReadRemoteFolderOperation(result);
        }else if(caller instanceof RemoveRemoteFileOperation){
            log("Remote Operation RemoveRemoteFileOperation finished");
            handleRemoveRemoteFileOperation(result);
        }else if(caller instanceof RenameRemoteFileOperation){
            log("Remote Operation RenameRemoteFileOperation finished");
            handleRenameRemoteFileOperation(result);
        }else if(caller instanceof UploadRemoteFileOperation){
            log("Remote Operation UploadRemoteFileOperation finished");
            handleUploadRemoteFileOperation(result);
        }else
        // Shares
        if(caller instanceof CreateRemoteShareOperation){
            log("Remote Operation CreateRemoteShareOperation finished");
            handleCreateRemoteShareOperationResult(result);
        }else if(caller instanceof GetRemoteShareesOperation){
            log("Remote Operation GetRemoteShareesOperation finished");
            handleGetRemoteShareesOperationResult(result);
        }else if(caller instanceof GetRemoteShareOperation){
            log("Remote Operation GetRemoteShareOperation finished");
            handleGetRemoteShareOperationResult(result);
        }else if(caller instanceof GetRemoteSharesForFileOperation){
            log("Remote Operation GetRemoteSharesForFileOperation finished");
            handleGetRemoteSharesForFileOperationResult(result);
        }else if(caller instanceof GetRemoteSharesOperation){
            log("Remote Operation GetRemoteSharesOperation finished");
            handleGetRemoteSharesOperationResult(result);
        }else if(caller instanceof RemoveRemoteShareOperation){
            log("Remote Operation RemoveRemoteShareOperation finished");
            handleRemoveRemoteShareOperationResult(result);
        }else if(caller instanceof UpdateRemoteShareOperation){
            log("Remote Operation UpdateRemoteShareOperation finished");
            handleUpdateRemoteShareOperationResult(result);
        }else
        // Status
        if(caller instanceof GetRemoteCapabilitiesOperation){
            log("Remote Operation GetRemoteCapabilitiesOperation finished");
            handleGetRemoteCapabilitiesOperationResult(result);
        }else if(caller instanceof GetRemoteStatusOperation){
            log("Remote Operation GetRemoteStatusOperation finished");
            handleGetRemoteStatusOperationResult(result);
        }else
        // User
        if(caller instanceof GetRemoteUserAvatarOperation){
            log("Remote Operation GetRemoteUserAvatarOperation finished");
            handleGetRemoteUserAvatarOperationResult(result);
        }else if(caller instanceof GetRemoteUserInfoOperation){
            log("Remote Operation GetRemoteUserInfoOperation finished");
            handleGetRemoteUserInfoOperationResult(result);
        }else if(caller instanceof GetRemoteUserQuotaOperation){
            log("Remote Operation GetRemoteUserQuotaOperation finished");
            handleGetRemoteUserQuotaOperationResult(result);
        }else
            log("Remote Operation not recognized: " + caller.getClass().getSimpleName());
    }

    @Override
    public void onTransferProgress(long progressRate, long totalTransferredSoFar, long totalToTransfer, String fileAbsoluteName){
        log("Transfer update (" + fileAbsoluteName + ") progressRate:" + progressRate + ", totalTransferredSoFar:" + totalTransferredSoFar + ", totalToTransfer:" + totalToTransfer);
    }

    // Logs & Info Methods
    //////////////////////

    private void log(String text){
        ((DefaultListModel<String>) mLstLog.getModel()).addElement(text);
        int lastIndex= mLstLog.getModel().getSize() - 1;
        if(lastIndex >= 0)
            mLstLog.ensureIndexIsVisible(lastIndex);
    }

    private void log(RemoteFile fileOrFolder){
        if(fileOrFolder != null){
            log("RemoteFile info");
            log("   RemotePath: " + fileOrFolder.getRemotePath());
            log("   MimeType: " + fileOrFolder.getMimeType());
            log("   Length: " + fileOrFolder.getLength());
            log("   CreationTimestamp: " + fileOrFolder.getCreationTimestamp());
            log("   ModifiedTimestamp: " + fileOrFolder.getModifiedTimestamp());
            log("   Etag: " + fileOrFolder.getEtag());
            log("   Permissions: " + fileOrFolder.getPermissions());
            log("   RemoteId: " + fileOrFolder.getRemoteId());
            log("   Size: " + fileOrFolder.getSize());
            log("   QuotaUsedBytes: " + fileOrFolder.getQuotaUsedBytes());
            log("   QuotaAvailableBytes: " + fileOrFolder.getQuotaAvailableBytes());
            log(" ");
        }
    }

    private void log(RemoteOperationResult result){
        if(result != null){
            log("RemoteOperationResult info");
            log("   AuthenticateHeader: " + result.getAuthenticateHeader());
            log("   Code: " + result.getCode().toString());
            log("   Exception: " + (result.getException() != null ? result.getException().toString() : "null"));
            log("   HttpCode: " + result.getHttpCode());
            log("   LastPermanentLocation: " + result.getLastPermanentLocation());
            log("   LogMessage: " + result.getLogMessage());
            log("   RedirectedLocation: " + result.getRedirectedLocation());
        }else
            log("Result is null");
    }

    private void clearLog(){
        DefaultListModel<String> listModel= (DefaultListModel<String>) mLstLog.getModel();
        listModel.removeAllElements();
    }

    private void clearUserData(){
        DefaultListModel<String> listModel= (DefaultListModel<String>) mLstUserData.getModel();
        listModel.removeAllElements();
    }

    private void showUserData(List<String> list){
        for(String item: list)
            log(item);
    }

    private void clearUserAvatar(){
        mLblUserAvatar.setIcon(null);
        mLblUserAvatar.setText("Avatar");
    }

    private void showUserAvatar(byte[] data, String token){
        if(data != null && data.length > 0){
            ImageIcon avatar= new ImageIcon(data, "eTag:" + token);
            mLblUserAvatar.setIcon(avatar);
            mLblUserAvatar.setText(avatar.getDescription());
        }
    }

    private void clearServerInfo(){
        DefaultListModel<String> listModel= (DefaultListModel<String>) mLstServerInfo.getModel();
        listModel.removeAllElements();
    }

    private void showServerInfo(List<String> list){
        for(String item: list)
            log(item);
    }

    private void showSharesMaster(String text){
        ((DefaultListModel<String>) mLstSharesMaster.getModel()).addElement(text);
    }

    private void clearSharesMaster(){
        DefaultListModel<String> listModel= (DefaultListModel<String>) mLstSharesMaster.getModel();
        listModel.removeAllElements();
    }

    private void showSharesDetail(String text){
        ((DefaultListModel<String>) mLstShareDetail.getModel()).addElement(text);
    }

    private void showSharesDetail(OCShare ocShare){
        if(ocShare != null){
            showSharesDetail("OCShare info");
            showSharesDetail("Id: " + ocShare.getId());
            showSharesDetail("FileSource: " + ocShare.getFileSource());
            showSharesDetail("ItemSource: " + ocShare.getItemSource());
            showSharesDetail("ShareType: " + ocShare.getShareType().name());
            showSharesDetail("ShareWith: " + ocShare.getShareWith());
            showSharesDetail("Path: " + ocShare.getPath());
            showSharesDetail("Permissions: " + ocShare.getPermissions());
            showSharesDetail("SharedDate: " + ocShare.getSharedDate());
            showSharesDetail("ExpirationDate: " + ocShare.getExpirationDate());
            showSharesDetail("Token: " + ocShare.getToken());
            showSharesDetail("SharedWithDisplayName: " + ocShare.getSharedWithDisplayName());
            showSharesDetail("IsFolder: " + ocShare.isFolder());
            showSharesDetail("UserId: " + ocShare.getUserId());
            showSharesDetail("RemoteId: " + ocShare.getRemoteId());
            showSharesDetail("ShareLink: " + ocShare.getShareLink());
        }else
            log("OCShare is null");
    }

    private void showSharesDetail(JSONObject object){
        if(object != null){
            showSharesDetail("Sharee info");
            showSharesDetail("   Label: " + object.getString("label"));
            showSharesDetail("   Shared With: " + object.getJSONObject("value").getString("shareWith"));
            showSharesDetail("   Share Type: " + ShareType.fromValue(object.getJSONObject("value").getInt("shareType")).name());
        }else
            log("Result is null");
    }

    private void clearSharesDetail(){
        DefaultListModel<String> listModel= (DefaultListModel<String>) mLstShareDetail.getModel();
        listModel.removeAllElements();
    }

    private void clearSharesUpdateShareData(){
        mSpinnerSharesRemoteIdToUpdate.setValue(0);
        mBtnSharesUpdateShare.setEnabled(false);
    }

    // ownCloud API Calls : Login
    /////////////////////////////

    // ownCloud API Calls : Files
    /////////////////////////////

    private void handleChunkedUploadRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){

            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleCopyRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                refreshFilesTree();
            else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleDownloadRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                log("File downloaded to: " + mTxtFldSyncDirectory.getText());
            else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleExistenceCheckRemoteOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                log("Element found");
            else
                log("Element not found");
        }else
            log("Result is null");
    }

    private void handleMoveRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                refreshFilesTree();
            else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleReadRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                RemoteFile file= (RemoteFile) result.getData().get(0);
                log(file);
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleReadRemoteFolderOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                // TODO: DFL
                // List<RemoteFile> filesAndFolders=
                // toRemoteFiles(result.getData());
                // DefaultTreeModel treeModel= (DefaultTreeModel)
                // mTreeDirectories.getModel();
                // buildRemoteDirectoriesTreeModel(treeModel,
                // mTreeDirectoriesSelectedNode, filesAndFolders,
                // mDirectoriesTreeElements);
                // mTreeDirectories.expandRow(0);
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleRemoveRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                refreshFilesTree();
            else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleRenameRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                refreshFilesTree();
            else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleUploadRemoteFileOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){

            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleCreateRemoteFolderOperation(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess())
                refreshFilesTree();
            else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    // ownCloud API calls : Shares
    //////////////////////////////

    private void handleCreateRemoteShareOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                log("Operation sucessfull");
                mBtnGetSharesForFile.doClick();
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteShareesOperationResult(RemoteOperationResult result){
        mSharesMasterObjects.clear();
        clearSharesDetail();
        clearSharesMaster();
        mCmbBxSharesCreateSharees.removeAllItems();
        if(result != null){
            if(result.isSuccess()){
                if(result.getData().size() > 0){
                    for(Object item: result.getData()){
                        OCSharee sharee= jsonToOCSharee((JSONObject) item);
                        showSharesMaster(sharee.getLabel());
                        mSharesMasterObjects.add(item);
                        ((DefaultComboBoxModel<OCSharee>) mCmbBxSharesCreateSharees.getModel()).addElement(sharee);
                    }
                    mLstSharesMaster.setSelectedIndex(0);
                }else
                    log("No data returned");
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteShareOperationResult(RemoteOperationResult result){
        mSharesMasterObjects.clear();
        clearSharesDetail();
        clearSharesMaster();
        if(result != null){
            if(result.isSuccess()){
                if(result.getData().size() > 0){
                    OCShare ocShare= null;
                    for(Object item: result.getData()){
                        ocShare= (OCShare) item;
                        showSharesMaster(Long.toString(ocShare.getId()) + ":" + ocShare.getSharedWithDisplayName());
                        mSharesMasterObjects.add(item);
                    }
                    mLstSharesMaster.setSelectedIndex(0);
                }else
                    log("No data returned");
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteSharesForFileOperationResult(RemoteOperationResult result){
        mSharesMasterObjects.clear();
        clearSharesDetail();
        clearSharesMaster();
        if(result != null){
            if(result.isSuccess()){
                if(result.getData().size() > 0){
                    OCShare ocShare= null;
                    for(Object item: result.getData()){
                        ocShare= (OCShare) item;
                        showSharesMaster(Long.toString(ocShare.getId()) + ":" + ocShare.getSharedWithDisplayName());
                        mSharesMasterObjects.add(item);
                    }
                    mLstSharesMaster.setSelectedIndex(0);
                }else
                    log("No data returned");
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteSharesOperationResult(RemoteOperationResult result){
        mSharesMasterObjects.clear();
        clearSharesDetail();
        clearSharesMaster();
        if(result != null){
            if(result.isSuccess()){
                if(result.getData().size() > 0){
                    OCShare ocShare= null;
                    for(Object item: result.getData()){
                        ocShare= (OCShare) item;
                        showSharesMaster(Long.toString(ocShare.getId()) + ":" + ocShare.getSharedWithDisplayName());
                        mSharesMasterObjects.add(ocShare);
                    }
                    mLstSharesMaster.setSelectedIndex(0);
                }else
                    log("No data returned");
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleRemoveRemoteShareOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                log("Operation sucessfull");
                mBtnGetSharesForFile.doClick();
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleUpdateRemoteShareOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                log("Operation sucessfull");
                if(result.getData().size() == 1)
                    log("Retorned Share update");
                mBtnGetSharesForFile.doClick();
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    // ownCloud API calls : Status
    //////////////////////////////

    private void handleGetRemoteCapabilitiesOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                List<String> toDisplay= new ArrayList<>();
                OCCapability ocCapability= null;
                int qtdData= result.getData().size();
                if(qtdData == 0)
                    toDisplay.add("no Avatar");
                else if(qtdData == 1){
                    ocCapability= (OCCapability) result.getData().get(0);
                    toDisplay.add("Id: " + ocCapability.getId());
                    toDisplay.add("AccountName: " + ocCapability.getAccountName());
                    toDisplay.add("VersionMayor: " + ocCapability.getVersionMayor());
                    toDisplay.add("VersionMinor: " + ocCapability.getVersionMinor());
                    toDisplay.add("VersionMicro: " + ocCapability.getVersionMicro());
                    toDisplay.add("VersionString: " + ocCapability.getVersionString());
                    toDisplay.add("VersionEdition: " + ocCapability.getVersionEdition());
                    toDisplay.add("CorePollinterval: " + ocCapability.getCorePollinterval());
                    toDisplay.add("FilesSharingApiEnabled: " + ocCapability.getFilesSharingApiEnabled());
                    toDisplay.add("FilesSharingPublicEnabled: " + ocCapability.getFilesSharingPublicEnabled());
                    toDisplay.add("FilesSharingPublicPasswordEnforced: " + ocCapability.getFilesSharingPublicPasswordEnforced());
                    toDisplay.add("FilesSharingPublicExpireDateEnabled: " + ocCapability.getFilesSharingPublicExpireDateEnabled());
                    toDisplay.add("FilesSharingPublicExpireDateDays: " + ocCapability.getFilesSharingPublicExpireDateDays());
                    toDisplay.add("FilesSharingPublicExpireDateEnforced: " + ocCapability.getFilesSharingPublicExpireDateEnforced());
                    toDisplay.add("FilesSharingPublicSendMail: " + ocCapability.getFilesSharingPublicSendMail());
                    toDisplay.add("FilesSharingPublicUpload: " + ocCapability.getFilesSharingPublicUpload());
                    toDisplay.add("FilesSharingUserSendMail: " + ocCapability.getFilesSharingUserSendMail());
                    toDisplay.add("FilesSharingResharing: " + ocCapability.getFilesSharingResharing());
                    toDisplay.add("FilesSharingFederationOutgoing: " + ocCapability.getFilesSharingFederationOutgoing());
                    toDisplay.add("FilesSharingFederationIncoming: " + ocCapability.getFilesSharingFederationIncoming());
                    toDisplay.add("FilesBigFileChuncking: " + ocCapability.getFilesBigFileChuncking());
                    toDisplay.add("FilesUndelete: " + ocCapability.getFilesUndelete());
                    toDisplay.add("FilesVersioning: " + ocCapability.getFilesVersioning());
                    showServerInfo(toDisplay);
                }
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteStatusOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                List<String> toDisplay= new ArrayList<>();
                OwnCloudVersion ownCloudVersion= null;
                int qtdData= result.getData().size();
                if(qtdData == 0)
                    toDisplay.add("no Status");
                else if(qtdData == 1){
                    ownCloudVersion= (OwnCloudVersion) result.getData().get(0);
                    toDisplay.add("Version: " + ownCloudVersion.getVersion());
                    toDisplay.add("IsVersionValid: " + ownCloudVersion.isVersionValid());
                }
                showServerInfo(toDisplay);
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    // ownCloud API calls : Users
    /////////////////////////////

    private void handleGetRemoteUserQuotaOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                int qtdData= result.getData().size();
                Quota quota= null;
                List<String> toDisplay= new ArrayList<>();
                if(qtdData == 0)
                    toDisplay.add("no result");
                else if(qtdData == 1){
                    quota= (Quota) result.getData().get(0);
                    toDisplay.add("   Free: " + humanReadableByteCount(quota.getFree(), false));
                    toDisplay.add("   Used: " + humanReadableByteCount(quota.getUsed(), false));
                    toDisplay.add("   Total: " + humanReadableByteCount(quota.getTotal(), false));
                    toDisplay.add("   Relative: " + humanReadableByteCount(new Double(quota.getRelative()).longValue(), false));
                }
                showUserData(toDisplay);
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteUserAvatarOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                List<String> toDisplay= new ArrayList<>();
                ResultData resultData= null;
                int qtdData= result.getData().size();
                if(qtdData == 0)
                    toDisplay.add("no Avatar");
                else if(qtdData == 1){
                    resultData= (ResultData) result.getData().get(0);
                    showUserAvatar(resultData.getAvatarData(), resultData.getEtag());
                }
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    private void handleGetRemoteUserInfoOperationResult(RemoteOperationResult result){
        if(result != null){
            if(result.isSuccess()){
                List<String> toDisplay= new ArrayList<>();
                UserInfo userInfo= null;
                int qtdData= result.getData().size();
                if(qtdData == 0)
                    toDisplay.add("no result");
                else if(qtdData == 1){
                    userInfo= (UserInfo) result.getData().get(0);
                    toDisplay.add("Id: " + userInfo.mId);
                    toDisplay.add("Display Name: " + userInfo.mDisplayName);
                    toDisplay.add("eMail: " + userInfo.mEmail);
                }
                showUserData(toDisplay);
            }else{
                log("Operation not sucessfull");
                log(result);
            }
        }else
            log("Result is null");
    }

    // ownCloud Helper Functions
    ////////////////////////////

    private List<RemoteFile> toRemoteFiles(List<?> uncastedList){
        List<RemoteFile> ret= new ArrayList<>();
        if(uncastedList != null)
            for(Object rf: uncastedList)
                ret.add((RemoteFile) rf);
        else
            log("Uncasted list is null");
        return ret;
    }

    private RemoteOperationResult executeRemoteOperation(RemoteOperation remoteOperation){
        RemoteOperationResult ret= null;
        if(remoteOperation != null)
            // log("Executing api call " +
            // remoteOperation.getClass().getSimpleName() + " synchronously");
            ret= remoteOperation.execute(mClient);
        else
            log("RemoteOperation is null");
        return ret;
    }

    private Thread executeRemoteOperation(RemoteOperation remoteOperation, OnRemoteOperationListener listener){
        Thread ret= null;
        if(remoteOperation != null)
            // log("Executing api call " +
            // remoteOperation.getClass().getSimpleName() + " asynchronously");
            ret= remoteOperation.execute(mClient, this, mExecutor);
        else
            log("RemoteOperation is null");
        return ret;
    }

    // Other
    ////////

    private OCSharee jsonToOCSharee(JSONObject json){
        OCSharee ret= null;
        if(json != null){
            ret= new OCSharee();
            if(json.has(OCSharee.JSON_KEY_LABEL) && !json.isNull(OCSharee.JSON_KEY_LABEL))
                ret.setLabel(json.getString(OCSharee.JSON_KEY_LABEL));
            if(json.has(OCSharee.JSON_KEY_VALUE) && !json.isNull(OCSharee.JSON_KEY_VALUE)){
                JSONObject inner= json.getJSONObject(OCSharee.JSON_KEY_VALUE);
                if(inner.has(OCSharee.JSON_KEY_SHAREWITH) && !inner.isNull(OCSharee.JSON_KEY_SHAREWITH))
                    ret.setShareWith(inner.getString(OCSharee.JSON_KEY_SHAREWITH));
                if(inner.has(OCSharee.JSON_KEY_SHARETYPE) && !inner.isNull(OCSharee.JSON_KEY_SHARETYPE))
                    ret.setShareType(ShareType.fromValue(inner.getInt(OCSharee.JSON_KEY_SHARETYPE)));
            }
        }
        return ret;
    }

    public void populateUpdateShareData(OCShare share){
        if(share != null){
            mSpinnerSharesRemoteIdToUpdate.setValue(share.getRemoteId());
            mTxtFldSharesUpdateExpirationDate.setText(Long.toString(share.getExpirationDate()));
            mChckBxSharesUpdateIsPublic.setSelected(!share.isPasswordProtected());
            setSelectedSharesUpdatePermissions(share.getPermissions());
            mBtnSharesUpdateShare.setEnabled(true);
        }
    }

    private String humanReadableByteCount(long bytes, boolean si){
        int unit= si ? 1000 : 1024;
        if(bytes < unit)
            return bytes + " B";
        int exp= (int) (Math.log(bytes) / Math.log(unit));
        String pre= (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private Path treePathToPath(TreePath treePath){
        StringBuilder sb= new StringBuilder();
        Object[] nodes= treePath.getPath();
        sb.append(FileUtils.PATH_SEPARATOR);
        for(int i= 1; i < nodes.length; i++)
            sb.append(nodes[i].toString()).append(FileUtils.PATH_SEPARATOR);
        return Paths.get(sb.toString());
    }

    private String treePathToWebDAVPath(TreePath treePath){
        StringBuilder sb= new StringBuilder();
        Object[] nodes= treePath.getPath();
        for(int i= 1; i < nodes.length; i++){
            sb.append(FileUtils.PATH_SEPARATOR);
            sb.append(nodes[i].toString());
        }
        return sb.toString();
    }

    private void refreshFilesTree(){
        // List root
        DefaultMutableTreeNode rootNode= new DefaultMutableTreeNode(FileUtils.PATH_SEPARATOR);
        DefaultTreeModel model= new DefaultTreeModel(rootNode);
        model.setAsksAllowsChildren(true);
        mFilesTreeFiles.setModel(model);
        mFilesTreeLoadedNodes.clear();
        mFilesTreeLoadedNodes.put(rootNode.toString(), true);
        ReadRemoteFolderOperation operation= new ReadRemoteFolderOperation(FileUtils.PATH_SEPARATOR);

        RemoteOperationResult ror= executeRemoteOperation(operation);
        List<RemoteFile> filesAndFolders= toRemoteFiles(ror.getData());
        buildTreeModel(model, (DefaultMutableTreeNode) model.getRoot(), filesAndFolders, mFilesTreeLoadedNodes, mFilesTreeElements);
        mFilesTreeFiles.expandRow(0);

        mFilesTreeFiles.setSelectionPath(new TreePath(mFilesTreeFiles.getModel().getRoot()));
    }

    private void refreshSharesTree(){
        // List root
        DefaultMutableTreeNode rootNode= new DefaultMutableTreeNode(FileUtils.PATH_SEPARATOR);
        DefaultTreeModel model= new DefaultTreeModel(rootNode);
        model.setAsksAllowsChildren(true);
        mSharesTreeFiles.setModel(model);
        mSharesTreeLoadedNodes.clear();
        mSharesTreeLoadedNodes.put(rootNode.toString(), true);
        ReadRemoteFolderOperation operation= new ReadRemoteFolderOperation(FileUtils.PATH_SEPARATOR);

        RemoteOperationResult ror= executeRemoteOperation(operation);
        List<RemoteFile> filesAndFolders= toRemoteFiles(ror.getData());
        buildTreeModel(model, (DefaultMutableTreeNode) model.getRoot(), filesAndFolders, mSharesTreeLoadedNodes, mSharesTreeElements);
        mSharesTreeFiles.expandRow(0);

        mSharesTreeFiles.setSelectionPath(new TreePath(mSharesTreeFiles.getModel().getRoot()));
    }

    private void buildTreeModel(DefaultTreeModel model, DefaultMutableTreeNode root, List<RemoteFile> filesAndFolders, Map<String, Boolean> loadedNodes, Set<OCTreeElements> ocTreeElements){
        if(filesAndFolders != null && !filesAndFolders.isEmpty()){
            DefaultMutableTreeNode node;
            for(RemoteFile rf: filesAndFolders)
                if("DIR".equalsIgnoreCase(rf.getMimeType()) && ocTreeElements.contains(OCTreeElements.DIRECTORY) || !"DIR".equalsIgnoreCase(rf.getMimeType()) && ocTreeElements.contains(OCTreeElements.FILE))
                    if(!root.toString().equalsIgnoreCase(rf.getRemotePath())){
                        Path remotePath= Paths.get(rf.getRemotePath());
                        node= new DefaultMutableTreeNode(remotePath.getFileName());
                        if(!"DIR".equalsIgnoreCase(rf.getMimeType()))
                            node.setAllowsChildren(false);
                        model.insertNodeInto(node, root, root.getChildCount());
                        loadedNodes.put(remotePath.toString(), false);

                    }
        }
    }

    private void setSelectedSharesUpdatePermissions(Integer value){
        final int MASK_READ= 1;
        final int MASK_UPDATE= 2;
        final int MASK_CREATE= 4;
        final int MASK_DELETE= 8;
        final int MASK_RESHARE= 16;

        mChckBxSharesUpdatePermissionRead.setSelected((value & MASK_READ) == MASK_READ);
        mChckBxSharesUpdatePermissionUpdate.setSelected((value & MASK_UPDATE) == MASK_UPDATE);
        mChckBxSharesUpdatePermissionCreate.setSelected((value & MASK_CREATE) == MASK_CREATE);
        mChckBxSharesUpdatePermissionDelete.setSelected((value & MASK_DELETE) == MASK_DELETE);
        mChckBxSharesUpdatePermissionReShare.setSelected((value & MASK_RESHARE) == MASK_RESHARE);
    }

    private int getSelectedSharesCreatePermissions(){
        int ret= 0;
        ret+= mChckBxSharesCreatePermissionRead.isSelected() ? 1 : 0;
        ret+= mChckBxSharesCreatePermissionUpdate.isSelected() ? 2 : 0;
        ret+= mChckBxSharesCreatePermissionCreate.isSelected() ? 4 : 0;
        ret+= mChckBxSharesCreatePermissionDelete.isSelected() ? 8 : 0;
        ret+= mChckBxSharesCreatePermissionReShare.isSelected() ? 16 : 0;
        return ret;
    }

    private int getSelectedSharesUpdatePermissions(){
        int ret= 0;
        ret+= mChckBxSharesUpdatePermissionRead.isSelected() ? 1 : 0;
        ret+= mChckBxSharesUpdatePermissionUpdate.isSelected() ? 2 : 0;
        ret+= mChckBxSharesUpdatePermissionCreate.isSelected() ? 4 : 0;
        ret+= mChckBxSharesUpdatePermissionDelete.isSelected() ? 8 : 0;
        ret+= mChckBxSharesUpdatePermissionReShare.isSelected() ? 16 : 0;
        return ret;
    }

    protected void applyCredentials(LoginMethod method){
        assert mClient != null;
        switch(method){
            case ANONYMOUS:
                mClient.setCredentials(OwnCloudCredentialsFactory.getAnonymousCredentials());
                break;
            case DEFAULT:
                mClient.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(
                        mTxtFldUser.getText(),
                        String.valueOf(mPassword.getPassword())));
                break;
            case TOKEN:
                mClient.setCredentials(OwnCloudCredentialsFactory.newBearerCredentials("token"));
                break;
            case SAML_SSO:
                mClient.setCredentials(OwnCloudCredentialsFactory.newSamlSsoCredentials(mTxtFldUser.getText(), "session-cookie"));
                break;
            case UNKNOWN:
            default:
                break;

        }
        log("Loggin with method " + method + ". Credentials set for " + mTxtFldUser.getText());
    }

    protected void revokeCredentials(){
        assert mClient != null;
        mClient.setCredentials(null);
        log("Credentials revoked");
    }

    protected void initializeHTTPClient(){
        Protocol pr= Protocol.getProtocol("https");
        if(pr == null || !(pr.getSocketFactory() instanceof SelfSignedConfidentSslSocketFactory))
            try{
                ProtocolSocketFactory psf= new SelfSignedConfidentSslSocketFactory();
                Protocol.registerProtocol(
                        "https",
                        new Protocol("https", psf, 443));

            }catch(GeneralSecurityException e){
                log("Self-signed confident SSL context could not be loaded");
            }

        try{
            URI serverUri= new URI(mTxtFldServerBaseURL.getText());

            mClient= new OwnCloudClient(serverUri, NetworkUtils.getMultiThreadedConnManager());
            mClient.setDefaultTimeouts(
                    OwnCloudClientFactory.DEFAULT_DATA_TIMEOUT,
                    OwnCloudClientFactory.DEFAULT_CONNECTION_TIMEOUT);
            mClient.setFollowRedirects(true);

            mClient.setBaseUri(serverUri);

            log("onCreate finished, ownCloud client ready");
        }catch(URISyntaxException e){
            e.printStackTrace();
        }

    }

    private void addPopup(Component component, final JPopupMenu popup){
        component.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent e){
                if(e.isPopupTrigger())
                    showMenu(e);
            }

            @Override
            public void mouseReleased(MouseEvent e){
                if(e.isPopupTrigger())
                    showMenu(e);
            }

            private void showMenu(MouseEvent e){
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        });
    }

    public List<RemoteFile> listRemoteFiles(String remotePath){
        List<RemoteFile> ret= new ArrayList<>();
        ReadRemoteFolderOperation readRemoteFolderOperation= new ReadRemoteFolderOperation(FileUtils.PATH_SEPARATOR);
        RemoteOperationResult result= readRemoteFolderOperation.execute(mClient);
        if(result.isSuccess() && result.getData().size() > 0){
            List<Object> remoteFilesAndFolders= result.getData();
            for(Object file: remoteFilesAndFolders){
                RemoteFile remoteFile= (RemoteFile) file;
                remoteFile= retrieveInfo(remoteFile);
                if("DIR".equalsIgnoreCase(remoteFile.getMimeType()))
                    ret.addAll(listRemoteFiles(remoteFile.getRemotePath()));
                else if("FILE".equalsIgnoreCase(remoteFile.getMimeType()))
                    ret.add(remoteFile);
            }
        }
        return ret;
    }

    // List all files and folders of a given path
    public RemoteFile retrieveInfo(RemoteFile remoteFile){
        RemoteFile ret= null;
        // Start from root folder
        ReadRemoteFileOperation readRemoteFileOperation= new ReadRemoteFileOperation(remoteFile.getRemotePath());
        RemoteOperationResult result= readRemoteFileOperation.execute(mClient);
        if(result.isSuccess() && result.getData().size() > 0)
            ret= (RemoteFile) result.getData().get(0);
        return ret;
    }

    public RemoteOperationResult getQuota(){
        GetRemoteUserQuotaOperation getUserQuotaOperation= new GetRemoteUserQuotaOperation();
        return getUserQuotaOperation.execute(mClient);
    }

    public RemoteOperationResult getUserAvatar(int dimension, String etag){
        GetRemoteUserAvatarOperation getUserAvatarOperation= new GetRemoteUserAvatarOperation(dimension, etag);
        return getUserAvatarOperation.execute(mClient);
    }

    /**
     * Extracts file from AssetManager to cache folder.
     *
     * @param   fileName    Name of the asset file to extract.
     * @return              File instance of the extracted file.
     */
    public File extractResource(String fileName) throws IOException{
        return extractResource(fileName, "/resources/files");
    }

    /**
     * Extracts file from AssetManager to cache folder.
     *
     * @param   fileName    Name of the asset file to extract.
     * @param   context     Android context to access assets and file system.
     * @return              File instance of the extracted file.
     */
    public File extractResource(String fileName, String resourcePath) throws IOException{
        File extractedFile= new File(System.getProperty("java.io.tmpdir") + File.separator + fileName);
        if(!extractedFile.exists()){
            InputStream in= null;
            FileOutputStream out= null;
            in= getClass().getResourceAsStream(resourcePath + File.separator + fileName);
            out= new FileOutputStream(extractedFile);
            byte[] buffer= new byte[BUFFER_SIZE];
            int readCount;
            while((readCount= in.read(buffer)) != -1)
                out.write(buffer, 0, readCount);
            out.flush();
            out.close();
            in.close();
        }
        return extractedFile;
    }

}
