package com.owncloud.java.lib.testclient;


public enum OCTreeElements{
    UNKNOWN,
    FILE,
    DIRECTORY;
}
