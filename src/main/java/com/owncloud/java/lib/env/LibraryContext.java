package com.owncloud.java.lib.env;

import com.owncloud.java.lib.account.AccountManager;

public class LibraryContext{

    private AccountManager mAccountManager;
    private String mKeyStorePath= "./";

    public String getKeyStorePath(){
        return mKeyStorePath;
    }

    public void setKeyStorePath(String path){
        mKeyStorePath= path;
    }

    public AccountManager getAccountManager(){
        return mAccountManager;
    }

    public void setAccountManager(AccountManager am){
        mAccountManager= am;
    }

}
