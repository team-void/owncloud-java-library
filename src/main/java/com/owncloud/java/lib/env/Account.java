package com.owncloud.java.lib.env;

import java.net.URI;

/**
 * Armazena os dados da conta do usuários
 * Será substituida pela classe aplicavel em  java. Provavelmente Credentials ( apache )
 * @author Diego
 *
 */
public class Account{

    public Account(){}

    public Account(Account source){
        setName(source.getName());
        setDisplayName(source.getDisplayName());
        setPassword(source.getPassword());
        setType(source.getType());
        setOCVersion(source.getOCVersion());
        setOCBaseURL(source.getOCBaseURL());
        setHasOAuth2Support(source.hasOAuth2Support());
        setOAuth2Token(source.getOAuth2Token());
        setHasSAML_WEB_SSOSupport(source.hasSAML_WEB_SSOSupport());
        setCookies(source.getCookies());
        setOCAccountVersion(source.getOCAccountVersion());
    }

    private String mName;
    private String mPassword;
    private String mType;

    /**Version should be 3 numbers separated by dot so it can be parsed by
     * {@link com.owncloud.java.lib.resources.status.OwnCloudVersion}*/
    private String mOCVersion;

    /**Base url should point to owncloud installation without trailing / ie:
     * http://server/path or https://owncloud.server*/
    private URI mOCBaseURL;

    /**Flag signaling if the ownCloud server can be accessed with OAuth2 access tokens.*/
    private boolean mHasOAuth2Support;

    private String mOAuth2Token;

    /**Flag signaling if the ownCloud server can be accessed with session cookies from SAML-based web single-sign-on.*/
    private boolean mHasSAML_WEB_SSOSupport;

    /**OC account cookies*/
    private String mCookies;

    /**OC account version*/
    private String mOCAccountVersion;

    /**User's display name*/
    private String mDisplayName;

    public void copyFrom(Account source){
        setName(source.getName());
        setDisplayName(source.getDisplayName());
        setPassword(source.getPassword());
        setType(source.getType());
        setOCVersion(source.getOCVersion());
        setOCBaseURL(source.getOCBaseURL());
        setHasOAuth2Support(source.hasOAuth2Support());
        setOAuth2Token(source.getOAuth2Token());
        setHasSAML_WEB_SSOSupport(source.hasSAML_WEB_SSOSupport());
        setCookies(source.getCookies());
        setOCAccountVersion(source.getOCAccountVersion());
    }

    public String getName(){
        return mName;
    }

    public void setName(String name){
        mName= name;
    }

    public String getPassword(){
        return mPassword;
    }

    public void setPassword(String password){
        mPassword= password;
    }

    public String getType(){
        return mType;
    }

    public void setType(String type){
        mType= type;
    }

    public String getOCVersion(){
        return mOCVersion;
    }

    public void setOCVersion(String version){
        mOCVersion= version;
    }

    public URI getOCBaseURL(){
        return mOCBaseURL;
    }

    public void setOCBaseURL(URI url){
        mOCBaseURL= url;
    }

    public boolean hasOAuth2Support(){
        return mHasOAuth2Support;
    }

    public void setHasOAuth2Support(boolean supports){
        mHasOAuth2Support= supports;
    }

    public String getOAuth2Token(){
        return mOAuth2Token;
    }

    public void setOAuth2Token(String token){
        mOAuth2Token= token;
    }

    public boolean hasSAML_WEB_SSOSupport(){
        return mHasSAML_WEB_SSOSupport;
    }

    public void setHasSAML_WEB_SSOSupport(boolean supports){
        mHasSAML_WEB_SSOSupport= supports;
    }

    public String getCookies(){
        return mCookies;
    }

    public void setCookies(String cookies){
        mCookies= cookies;
    }

    public String getOCAccountVersion(){
        return mOCAccountVersion;
    }

    public void setOCAccountVersion(String version){
        mOCAccountVersion= version;
    }

    public String getDisplayName(){
        return mDisplayName;
    }

    public void setDisplayName(String name){
        mDisplayName= name;
    }

}
