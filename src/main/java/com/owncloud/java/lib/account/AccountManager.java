package com.owncloud.java.lib.account;

import java.util.List;
import java.util.Map;

import com.owncloud.java.lib.common.OwnCloudAccount;
import com.owncloud.java.lib.env.Account;

public interface AccountManager{

    public void saveAccount(Account account);

    public void saveAccounts(List<Account> accounts);

    public void removeAccount(Account account);

    public Account getAccount(String accountName);

    public Account getAccount(String accountName, Account account);

    public Account getAccount(OwnCloudAccount ocAccount);

    public List<Account> getAccounts();

    public List<Account> getAccountsByType(String type);

    public Map<String, String> getAuthTokens(String accountName);

    public String getAuthToken(String accountName, String type);

    public void invalidateAuthToken(String type, String authToken);

    public void clearPassword(String accountName);

}