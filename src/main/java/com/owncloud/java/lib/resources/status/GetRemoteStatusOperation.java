/*
 * ownCloud Android Library is available under MIT license Copyright (C) 2016
 * ownCloud GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.owncloud.java.lib.resources.status;

import java.net.URI;
import java.util.ArrayList;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.owncloud.java.lib.common.OwnCloudClient;
import com.owncloud.java.lib.common.OwnCloudClientManagerFactory;
import com.owncloud.java.lib.common.accounts.AccountUtils;
import com.owncloud.java.lib.common.operations.RemoteOperation;
import com.owncloud.java.lib.common.operations.RemoteOperationResult;
import com.owncloud.java.lib.common.utils.Log_OC;

/**
 * Checks if the server is valid and if the server supports the Share API
 *
 * @author David A. Velasco
 * @author masensio
 *
 */

public class GetRemoteStatusOperation extends RemoteOperation{

    /**
     * Maximum time to wait for a response from the server when the connection is being tested,
     * in MILLISECONDs.
     */
    public static final int TRY_CONNECTION_TIMEOUT= 5000;

    private static final String TAG= GetRemoteStatusOperation.class.getSimpleName();

    private static final String NODE_INSTALLED= "installed";
    private static final String NODE_VERSION= "version";

    private RemoteOperationResult mLatestResult;

    public GetRemoteStatusOperation(){
        //
    }

    private boolean tryConnection(OwnCloudClient client){
        boolean retval= false;
        GetMethod get= null;
        String baseUrlSt= client.getBaseUri().toString();
        try{
            get= new GetMethod(baseUrlSt + AccountUtils.STATUS_PATH);

            HttpParams params= HttpMethodParams.getDefaultParams();
            params.setParameter(HttpMethodParams.USER_AGENT,
                    OwnCloudClientManagerFactory.getUserAgent());
            get.getParams().setDefaults(params);

            client.setFollowRedirects(false);
            boolean isRedirectToNonSecureConnection= false;
            int status= client.executeMethod(get, TRY_CONNECTION_TIMEOUT, TRY_CONNECTION_TIMEOUT);
            mLatestResult= new RemoteOperationResult(
                    status == HttpStatus.SC_OK,
                    status,
                    get.getResponseHeaders());

            String redirectedLocation= mLatestResult.getRedirectedLocation();
            while(redirectedLocation != null && redirectedLocation.length() > 0
                    && !mLatestResult.isSuccess()){

                isRedirectToNonSecureConnection|= baseUrlSt.startsWith("https://") &&
                        redirectedLocation.startsWith("http://");
                get.releaseConnection();
                get= new GetMethod(redirectedLocation);
                status= client.executeMethod(get, TRY_CONNECTION_TIMEOUT, TRY_CONNECTION_TIMEOUT);
                mLatestResult= new RemoteOperationResult(
                        status == HttpStatus.SC_OK,
                        status,
                        get.getResponseHeaders());
                redirectedLocation= mLatestResult.getRedirectedLocation();
            }

            String response= get.getResponseBodyAsString();
            if(status == HttpStatus.SC_OK){
                JSONObject json= new JSONObject(response);
                if(!json.getBoolean(NODE_INSTALLED))
                    mLatestResult= new RemoteOperationResult(
                            RemoteOperationResult.ResultCode.INSTANCE_NOT_CONFIGURED);
                else{
                    String version= json.getString(NODE_VERSION);
                    OwnCloudVersion ocVersion= new OwnCloudVersion(version);
                    if(!ocVersion.isVersionValid())
                        mLatestResult= new RemoteOperationResult(
                                RemoteOperationResult.ResultCode.BAD_OC_VERSION);
                    else{
                        // success
                        if(isRedirectToNonSecureConnection)
                            mLatestResult= new RemoteOperationResult(
                                    RemoteOperationResult.ResultCode.OK_REDIRECT_TO_NON_SECURE_CONNECTION);
                        else
                            mLatestResult= new RemoteOperationResult(
                                    baseUrlSt.startsWith("https://") ? RemoteOperationResult.ResultCode.OK_SSL : RemoteOperationResult.ResultCode.OK_NO_SSL);

                        ArrayList<Object> data= new ArrayList<>();
                        data.add(ocVersion);
                        mLatestResult.setData(data);
                        retval= true;
                    }
                }

            }else
                mLatestResult= new RemoteOperationResult(false, status, get.getResponseHeaders());

        }catch(JSONException e){
            mLatestResult= new RemoteOperationResult(
                    RemoteOperationResult.ResultCode.INSTANCE_NOT_CONFIGURED);

        }catch(Exception e){
            mLatestResult= new RemoteOperationResult(e);

        }finally{
            if(get != null)
                get.releaseConnection();
        }

        if(mLatestResult.isSuccess())
            Log_OC.i(TAG, "Connection check at " + baseUrlSt + ": " + mLatestResult.getLogMessage());
        else if(mLatestResult.getException() != null)
            Log_OC.e(TAG, "Connection check at " + baseUrlSt + ": " + mLatestResult.getLogMessage(),
                    mLatestResult.getException());
        else
            Log_OC.e(TAG, "Connection check at " + baseUrlSt + ": " + mLatestResult.getLogMessage());

        return retval;
    }

    @Override
    protected RemoteOperationResult run(OwnCloudClient client){
        String baseUriStr= client.getBaseUri().toString();
        if(baseUriStr.startsWith("http://") || baseUriStr.startsWith("https://"))
            tryConnection(client);
        else{
            client.setBaseUri(URI.create("https://" + baseUriStr));
            boolean httpsSuccess= tryConnection(client);
            if(!httpsSuccess && !mLatestResult.isSslRecoverableException()){
                Log_OC.d(TAG, "establishing secure connection failed, trying non secure connection");
                client.setBaseUri(URI.create("http://" + baseUriStr));
                tryConnection(client);
            }
        }
        return mLatestResult;
    }

}
