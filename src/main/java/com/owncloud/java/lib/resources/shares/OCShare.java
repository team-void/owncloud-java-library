/*
 * ownCloud Android Library is available under MIT license Copyright (C) 2016
 * ownCloud GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.owncloud.java.lib.resources.shares;

import java.io.Serializable;

import com.owncloud.java.lib.common.utils.Log_OC;
import com.owncloud.java.lib.resources.files.FileUtils;

/**
 * Contains the data of a Share from the Share API
 *
 * @author masensio
 *
 */
public class OCShare implements Serializable{

    /**
     * Generated - should be refreshed every time the class changes!!
     */
    private static final long serialVersionUID= 4124975224281327921L;

    private static final String TAG= OCShare.class.getSimpleName();

    public static final int DEFAULT_PERMISSION= -1;
    public static final int READ_PERMISSION_FLAG= 1;
    public static final int UPDATE_PERMISSION_FLAG= 2;
    public static final int CREATE_PERMISSION_FLAG= 4;
    public static final int DELETE_PERMISSION_FLAG= 8;
    public static final int SHARE_PERMISSION_FLAG= 16;
    public static final int MAXIMUM_PERMISSIONS_FOR_FILE= READ_PERMISSION_FLAG +
            UPDATE_PERMISSION_FLAG +
            SHARE_PERMISSION_FLAG;
    public static final int MAXIMUM_PERMISSIONS_FOR_FOLDER= MAXIMUM_PERMISSIONS_FOR_FILE +
            CREATE_PERMISSION_FLAG +
            DELETE_PERMISSION_FLAG;
    public static final int FEDERATED_PERMISSIONS_FOR_FILE_UP_TO_OC9= READ_PERMISSION_FLAG +
            UPDATE_PERMISSION_FLAG;
    public static final int FEDERATED_PERMISSIONS_FOR_FILE_AFTER_OC9= READ_PERMISSION_FLAG +
            UPDATE_PERMISSION_FLAG +
            SHARE_PERMISSION_FLAG;
    public static final int FEDERATED_PERMISSIONS_FOR_FOLDER_UP_TO_OC9= READ_PERMISSION_FLAG +
            UPDATE_PERMISSION_FLAG +
            CREATE_PERMISSION_FLAG +
            DELETE_PERMISSION_FLAG;
    public static final int FEDERATED_PERMISSIONS_FOR_FOLDER_AFTER_OC9= FEDERATED_PERMISSIONS_FOR_FOLDER_UP_TO_OC9 +
            SHARE_PERMISSION_FLAG;

    private long mId;
    private long mFileSource;
    private long mItemSource;
    private ShareType mShareType;
    private String mShareWith;
    private String mPath;
    private int mPermissions;
    private long mSharedDate;
    private long mExpirationDate;
    private String mToken;
    private String mSharedWithDisplayName;
    private boolean mIsFolder;
    private long mUserId;
    private long mRemoteId;
    private String mShareLink;

    public OCShare(){
        super();
        resetData();
    }

    public OCShare(String path){
        resetData();
        if(path == null || path.length() <= 0 || !path.startsWith(FileUtils.PATH_SEPARATOR)){
            Log_OC.e(TAG, "Trying to create a OCShare with a non valid path");
            throw new IllegalArgumentException("Trying to create a OCShare with a non valid path: " + path);
        }
        mPath= path;
    }

    @Override
    public String toString(){
        String newLine= System.getProperty("line.separator");
        StringBuilder sb= new StringBuilder();
        sb.append("Id: " + getId() + newLine);
        sb.append("FileSource: " + getFileSource() + newLine);
        sb.append("ItemSource: " + getItemSource() + newLine);
        sb.append("ShareType: " + getShareType().name() + newLine);
        sb.append("ShareWith: " + getShareWith() + newLine);
        sb.append("Path: " + getPath() + newLine);
        sb.append("Permissions: " + getPermissions() + newLine);
        sb.append("SharedDate: " + getSharedDate() + newLine);
        sb.append("ExpirationDate: " + getExpirationDate() + newLine);
        sb.append("Token: " + getToken() + newLine);
        sb.append("SharedWithDisplayName: " + getSharedWithDisplayName() + newLine);
        sb.append("IsFolder: " + isFolder() + newLine);
        sb.append("UserId: " + getUserId() + newLine);
        sb.append("RemoteId: " + getRemoteId() + newLine);
        sb.append("ShareLink: " + getShareLink());
        return sb.toString();
    }

    /**
     * Used internally. Reset all file properties
     */
    private void resetData(){
        mId= -1;
        mFileSource= 0;
        mItemSource= 0;
        mShareType= ShareType.NO_SHARED;
        mShareWith= "";
        mPath= "";
        mPermissions= -1;
        mSharedDate= 0;
        mExpirationDate= 0;
        mToken= "";
        mSharedWithDisplayName= "";
        mIsFolder= false;
        mUserId= -1;
        mRemoteId= -1;
        mShareLink= "";
    }

    /// Getters and Setters

    public long getId(){
        return mId;
    }

    public void setId(long id){
        mId= id;
    }

    public long getFileSource(){
        return mFileSource;
    }

    public void setFileSource(long fileSource){
        mFileSource= fileSource;
    }

    public long getItemSource(){
        return mItemSource;
    }

    public void setItemSource(long itemSource){
        mItemSource= itemSource;
    }

    public ShareType getShareType(){
        return mShareType;
    }

    public void setShareType(ShareType shareType){
        mShareType= shareType;
    }

    public String getShareWith(){
        return mShareWith;
    }

    public void setShareWith(String shareWith){
        mShareWith= shareWith != null ? shareWith : "";
    }

    public String getPath(){
        return mPath;
    }

    public void setPath(String path){
        mPath= path != null ? path : "";
    }

    public int getPermissions(){
        return mPermissions;
    }

    public void setPermissions(int permissions){
        mPermissions= permissions;
    }

    public long getSharedDate(){
        return mSharedDate;
    }

    public void setSharedDate(long sharedDate){
        mSharedDate= sharedDate;
    }

    public long getExpirationDate(){
        return mExpirationDate;
    }

    public void setExpirationDate(long expirationDate){
        mExpirationDate= expirationDate;
    }

    public String getToken(){
        return mToken;
    }

    public void setToken(String token){
        mToken= token != null ? token : "";
    }

    public String getSharedWithDisplayName(){
        return mSharedWithDisplayName;
    }

    public void setSharedWithDisplayName(String sharedWithDisplayName){
        mSharedWithDisplayName= sharedWithDisplayName != null ? sharedWithDisplayName : "";
    }

    public boolean isFolder(){
        return mIsFolder;
    }

    public void setIsFolder(boolean isFolder){
        mIsFolder= isFolder;
    }

    public long getUserId(){
        return mUserId;
    }

    public void setUserId(long userId){
        mUserId= userId;
    }

    public long getRemoteId(){
        return mRemoteId;
    }

    public void setIdRemoteShared(long remoteId){
        mRemoteId= remoteId;
    }

    public String getShareLink(){
        return mShareLink;
    }

    public void setShareLink(String shareLink){
        mShareLink= shareLink != null ? shareLink : "";
    }

    public boolean isPasswordProtected(){
        return ShareType.PUBLIC_LINK.equals(mShareType) && mShareWith.length() > 0;
    }

}
