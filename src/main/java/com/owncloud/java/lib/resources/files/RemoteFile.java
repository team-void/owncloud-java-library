/*
 * ownCloud Android Library is available under MIT license Copyright (C) 2016
 * ownCloud GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.owncloud.java.lib.resources.files;

import java.io.Serializable;
import java.math.BigDecimal;

import com.owncloud.java.lib.common.network.WebdavEntry;

/**
 *  Contains the data of a Remote File from a WebDavEntry
 *
 *  @author masensio
 */

public class RemoteFile implements Serializable{

    /** Generated - should be refreshed every time the class changes!! */
    private static final long serialVersionUID= 3130865437811248451L;

    private String mRemotePath;
    private String mMimeType;
    private long mLength;
    private long mCreationTimestamp;
    private long mModifiedTimestamp;
    private String mEtag;
    private String mPermissions;
    private String mRemoteId;
    private long mSize;
    private BigDecimal mQuotaUsedBytes;
    private BigDecimal mQuotaAvailableBytes;

    /**
     * Getters and Setters
     */

    public String getRemotePath(){
        return mRemotePath;
    }

    public void setRemotePath(String remotePath){
        mRemotePath= remotePath;
    }

    public String getMimeType(){
        return mMimeType;
    }

    public void setMimeType(String mimeType){
        mMimeType= mimeType;
    }

    public long getLength(){
        return mLength;
    }

    public void setLength(long length){
        mLength= length;
    }

    public long getCreationTimestamp(){
        return mCreationTimestamp;
    }

    public void setCreationTimestamp(long creationTimestamp){
        mCreationTimestamp= creationTimestamp;
    }

    public long getModifiedTimestamp(){
        return mModifiedTimestamp;
    }

    public void setModifiedTimestamp(long modifiedTimestamp){
        mModifiedTimestamp= modifiedTimestamp;
    }

    public String getEtag(){
        return mEtag;
    }

    public void setEtag(String etag){
        mEtag= etag;
    }

    public String getPermissions(){
        return mPermissions;
    }

    public void setPermissions(String permissions){
        mPermissions= permissions;
    }

    public String getRemoteId(){
        return mRemoteId;
    }

    public void setRemoteId(String remoteId){
        mRemoteId= remoteId;
    }

    public long getSize(){
        return mSize;
    }

    public void setSize(long size){
        mSize= size;
    }

    public BigDecimal getQuotaUsedBytes(){
        return mQuotaUsedBytes;
    }

    public void setQuotaUsedBytes(BigDecimal quotaUsedBytes){
        mQuotaUsedBytes= quotaUsedBytes;
    }

    public BigDecimal getQuotaAvailableBytes(){
        return mQuotaAvailableBytes;
    }

    public void setQuotaAvailableBytes(BigDecimal quotaAvailableBytes){
        mQuotaAvailableBytes= quotaAvailableBytes;
    }

    public RemoteFile(){
        resetData();
    }

    /**
     * Create new {@link RemoteFile} with given path.
     *
     * The path received must be URL-decoded. Path separator must be OCFile.PATH_SEPARATOR, and it must be the first character in 'path'.
     *
     * @param path The remote path of the file.
     */
    public RemoteFile(String path){
        resetData();
        if(path == null || path.length() <= 0 || !path.startsWith(FileUtils.PATH_SEPARATOR))
            throw new IllegalArgumentException("Trying to create a OCFile with a non valid remote path: " + path);
        mRemotePath= path;
    }

    public RemoteFile(WebdavEntry we){
        this(we.decodedPath());
        setCreationTimestamp(we.createTimestamp());
        setLength(we.contentLength());
        setMimeType(we.contentType());
        setModifiedTimestamp(we.modifiedTimestamp());
        setEtag(we.etag());
        setPermissions(we.permissions());
        setRemoteId(we.remoteId());
        setSize(we.size());
        setQuotaUsedBytes(we.quotaUsedBytes());
        setQuotaAvailableBytes(we.quotaAvailableBytes());
    }

    /**
     * Used internally. Reset all file properties
     */
    private void resetData(){
        mRemotePath= null;
        mMimeType= null;
        mLength= 0;
        mCreationTimestamp= 0;
        mModifiedTimestamp= 0;
        mEtag= null;
        mPermissions= null;
        mRemoteId= null;
        mSize= 0;
        mQuotaUsedBytes= null;
        mQuotaAvailableBytes= null;
    }

}
