package com.owncloud.java.lib.exceptions;

public class AccountsException extends Exception{

    /** */
    private static final long serialVersionUID= 2522648897728109130L;

    public AccountsException(){

    }

    public AccountsException(String message, Throwable cause){
        super(message, cause);
    }

}
