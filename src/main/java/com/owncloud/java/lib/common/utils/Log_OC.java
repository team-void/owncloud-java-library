package com.owncloud.java.lib.common.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class Log_OC{

    public enum LogMode{
        UNKNOWN, DEBUG, VERBOSE;

        public static LogMode fromName(String name){
            LogMode ret= UNKNOWN;
            if(name != null)
                try{
                    ret= LogMode.valueOf(name);
                }catch(Exception ex){
                    ret= UNKNOWN;
                }
            return ret;
        }

        public static List<String> getNames(){
            List<String> ret= new ArrayList<>(LogMode.values().length);
            for(LogMode status: LogMode.values())
                ret.add(status.name());
            return ret;
        }
    }

    private static final String SIMPLE_DATE_FORMAT= "yyyy/MM/dd HH:mm:ss";
    private static final String LOG_FOLDER_NAME= "log";
    private static final long MAX_FILE_SIZE= 1000000; // 1MB

    private static String mOwncloudDataFolderLog= "owncloud_log";

    private static File mLogFile;
    private static File mFolder;
    private static BufferedWriter mBuf;

    private static String[] mLogFileNames= {"currentLog.txt", "olderLog.txt"};

    private static boolean isMaxFileSizeReached= false;
    private static boolean isEnabled= false;

    private static final String LOG_MSG_SEPARATOR= " : ";
    private static final String LOG_MSG_TYPE_I= "[I]";
    private static final String LOG_MSG_TYPE_D= "[D]";
    private static final String LOG_MSG_TYPE_E= "[E]";
    private static final String LOG_MSG_TYPE_V= "[V]";
    private static final String LOG_MSG_TYPE_W= "[W]";
    private static final String LOG_MSG_TYPE_WTF= "[WTF]";

    private static Set<LogMode> mLogModes= new HashSet<>(Arrays.asList(LogMode.DEBUG, LogMode.VERBOSE));

    public static void setLogDataFolder(String logFolder){
        mOwncloudDataFolderLog= logFolder;
    }

    public static final Set<LogMode> getLogModes(){
        return mLogModes;
    }

    public static boolean isLoggable(LogMode mode){
        return mLogModes.contains(mode);
    }

    public static void i(String TAG, String message){
        System.out.println(LOG_MSG_TYPE_I + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
        appendLog(LOG_MSG_TYPE_I + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
    }

    public static void d(String TAG, String message){
        System.out.println(LOG_MSG_TYPE_D + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
        appendLog(LOG_MSG_TYPE_D + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
    }

    public static void d(String TAG, String message, Exception e){
        System.out.println(LOG_MSG_TYPE_D + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message + LOG_MSG_SEPARATOR + (e != null ? e.toString() : "NULL"));
        appendLog(LOG_MSG_TYPE_D + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message + " Exception : " + (e != null ? e.toString() : "NULL"));
    }

    public static void e(String TAG, String message){
        System.out.println(LOG_MSG_TYPE_E + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
        appendLog(LOG_MSG_TYPE_E + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
    }

    public static void e(String TAG, String message, Throwable e){
        System.out.println(LOG_MSG_TYPE_E + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message + LOG_MSG_SEPARATOR + (e != null ? e.toString() : "NULL"));
        appendLog(LOG_MSG_TYPE_E + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message + " Exception : " + (e != null ? e.toString() : "NULL"));
    }

    public static void v(String TAG, String message){
        System.out.println(LOG_MSG_TYPE_V + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
        appendLog(LOG_MSG_TYPE_V + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
    }

    public static void w(String TAG, String message){
        System.out.println(LOG_MSG_TYPE_W + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
        appendLog(LOG_MSG_TYPE_W + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
    }

    public static void wtf(String TAG, String message){
        System.out.println(LOG_MSG_TYPE_WTF + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
        appendLog(LOG_MSG_TYPE_WTF + LOG_MSG_SEPARATOR + TAG + LOG_MSG_SEPARATOR + message);
    }

    /**
     * Start doing logging
     * @param storagePath : directory for keeping logs
     */
    synchronized public static void startLogging(String storagePath){
        String logPath= storagePath + File.separator + mOwncloudDataFolderLog + File.separator + LOG_FOLDER_NAME;
        mFolder= new File(logPath);
        mLogFile= new File(mFolder + File.separator + mLogFileNames[0]);

        boolean isFileCreated= false;

        if(!mFolder.exists()){
            mFolder.mkdirs();
            isFileCreated= true;
            System.out.println("LOG_OC" + LOG_MSG_SEPARATOR + "Log file created");
        }

        try{

            // Create the current log file if does not exist
            mLogFile.createNewFile();
            mBuf= new BufferedWriter(new FileWriter(mLogFile, true));
            isEnabled= true;

            if(isFileCreated){
                appendSystemProperties();
                // appendUserEnvironmentVariables();
            }

        }catch(IOException e){
            e.printStackTrace();
        }finally{
            if(mBuf != null)
                try{
                    mBuf.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
        }
    }

    synchronized public static void stopLogging(){
        try{
            if(mBuf != null)
                mBuf.close();
            isEnabled= false;

            mLogFile= null;
            mFolder= null;
            mBuf= null;
            isMaxFileSizeReached= false;
            isEnabled= false;

        }catch(IOException e){
            // Because we are stopping logging, we only log to Android console.
            System.out.println(LOG_MSG_TYPE_E + LOG_MSG_SEPARATOR + "OC_Log" + LOG_MSG_SEPARATOR + "Closing log file failed: " + LOG_MSG_SEPARATOR + (e != null ? e.toString() : "NULL"));
        }catch(Exception e){
            // This catch should never fire because we do null check on mBuf.
            // But just for the sake of stability let's log this odd situation.
            // Because we are stopping logging, we only log to Android console.
            System.out.println(LOG_MSG_TYPE_E + LOG_MSG_SEPARATOR + "OC_Log" + LOG_MSG_SEPARATOR + "Stopping logging failed: " + LOG_MSG_SEPARATOR + (e != null ? e.toString() : "NULL"));
        }
    }

    /**
     * Delete history logging
     */
    public static void deleteHistoryLogging(){
        File folderLogs= new File(mFolder + File.separator);
        if(folderLogs.isDirectory()){
            String[] myFiles= folderLogs.list();
            if(myFiles != null)
                for(int i= 0; i < myFiles.length; i++){
                    File myFile= new File(folderLogs, myFiles[i]);
                    myFile.delete();
                }
        }
    }

    /**
     * Append the local user environment variables
     */
    @SuppressWarnings("unused")
    private static void appendUserEnvironmentVariables(){
        Map<String, String> envs= System.getenv();
        Set<Entry<String, String>> entries= envs.entrySet();
        for(Map.Entry<String, String> entry: entries)
            appendLog(entry.getKey() + LOG_MSG_SEPARATOR + entry.getValue());
    }

    /**
     * Append the info of the machine
     */
    private static void appendSystemProperties(){
        Properties props= System.getProperties();
        Set<Entry<Object, Object>> keys= props.entrySet();
        for(Map.Entry<Object, Object> key: keys)
            System.out.println(key.getKey() + ":" + key.getValue());
    }

    /**
     * Append to the log file the info passed
     * @param text : text for adding to the log file
     */
    synchronized private static void appendLog(String text){

        if(isEnabled){

            if(isMaxFileSizeReached){

                // Move current log file info to another file (old logs)
                File olderFile= new File(mFolder + File.separator + mLogFileNames[1]);
                if(mLogFile.exists())
                    mLogFile.renameTo(olderFile);

                // Construct a new file for current log info
                mLogFile= new File(mFolder + File.separator + mLogFileNames[0]);
                isMaxFileSizeReached= false;
            }

            String timeStamp= new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(Calendar.getInstance().getTime());

            try{
                mBuf= new BufferedWriter(new FileWriter(mLogFile, true));
                mBuf.newLine();
                mBuf.write(timeStamp);
                mBuf.newLine();
                mBuf.write(text);
                mBuf.newLine();
            }catch(IOException e){
                e.printStackTrace();
            }finally{
                try{
                    mBuf.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }

            // Check if current log file size is bigger than the max file size
            // defined
            if(mLogFile.length() > MAX_FILE_SIZE)
                isMaxFileSizeReached= true;
        }
    }

    public static String[] getLogFileNames(){
        return mLogFileNames;
    }
}
