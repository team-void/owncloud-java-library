/*
 * ownCloud Android Library is available under MIT license Copyright (C) 2016
 * ownCloud GmbH. Copyright (C) 2012 Bartek Przybylski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.owncloud.java.lib.common.accounts;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.httpclient.Cookie;

import com.owncloud.java.lib.account.AccountManager;
import com.owncloud.java.lib.common.OwnCloudClient;
import com.owncloud.java.lib.common.OwnCloudCredentials;
import com.owncloud.java.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.java.lib.common.utils.Log_OC;
import com.owncloud.java.lib.env.Account;
import com.owncloud.java.lib.exceptions.AccountsException;
import com.owncloud.java.lib.exceptions.AuthenticatorException;
import com.owncloud.java.lib.exceptions.OperationCanceledException;
import com.owncloud.java.lib.resources.status.OwnCloudVersion;

public class AccountUtils{

    private static final String TAG= AccountUtils.class.getSimpleName();

    public static final String WEBDAV_PATH_1_2= "/webdav/owncloud.php";
    public static final String WEBDAV_PATH_2_0= "/files/webdav.php";
    public static final String WEBDAV_PATH_4_0= "/remote.php/webdav";
    public static final String ODAV_PATH= "/remote.php/odav";
    private static final String SAML_SSO_PATH= "/remote.php/webdav";
    public static final String CARDDAV_PATH_2_0= "/apps/contacts/carddav.php";
    public static final String CARDDAV_PATH_4_0= "/remote/carddav.php";
    public static final String STATUS_PATH= "/status.php";

    /**
     * Returns the proper URL path to access the WebDAV interface of an ownCloud server,
     * according to its version and the authorization method used.
     *
     * @param	version         	Version of ownCloud server.
     * @param 	supportsOAuth		If true, access with OAuth 2 authorization is considered.
     * @param 	supportsSamlSso		If true, and supportsOAuth is false, access with SAML-based single-sign-on is considered.
     * @return 						WebDAV path for given OC version, null if OC version unknown
     */
    public static String getWebdavPath(OwnCloudVersion version, boolean supportsOAuth, boolean supportsSamlSso){
        if(version != null){
            if(supportsOAuth)
                return ODAV_PATH;
            if(supportsSamlSso)
                return SAML_SSO_PATH;
            if(version.compareTo(OwnCloudVersion.owncloud_v4) >= 0)
                return WEBDAV_PATH_4_0;
            if(version.compareTo(OwnCloudVersion.owncloud_v3) >= 0
                    || version.compareTo(OwnCloudVersion.owncloud_v2) >= 0)
                return WEBDAV_PATH_2_0;
            if(version.compareTo(OwnCloudVersion.owncloud_v1) >= 0)
                return WEBDAV_PATH_1_2;
        }
        return null;
    }

    /**
     * Get the username corresponding to an OC account.
     *
     * @param account   An OC account
     * @return          Username for the given account, extracted from the account.name
     */
    public static String getUsernameForAccount(Account account){
        String username= null;
        try{
            username= account.getName().substring(0, account.getName().lastIndexOf('@'));
        }catch(Exception e){
            Log_OC.e(TAG, "Couldn't get a username for the given account", e);
        }
        return username;
    }

    /**
     *
     * @return
     * @throws IOException
     * @throws AuthenticatorException
     * @throws OperationCanceledException
     */
    public static OwnCloudCredentials getCredentialsForAccount(AccountManager am, Account account)
            throws OperationCanceledException, AuthenticatorException, IOException{
        OwnCloudCredentials credentials= null;

        boolean isOauth2= account.hasOAuth2Support();
        boolean isSamlSso= account.hasSAML_WEB_SSOSupport();
        String username= account.getName();

        if(isOauth2){
            String accessToken= am.getAuthToken(account.getName(), AccountTypeUtils.getAuthTokenTypeAccessToken(account.getType()));
            credentials= OwnCloudCredentialsFactory.newBearerCredentials(accessToken);
        }else if(isSamlSso){
            String accessToken= am.getAuthToken(account.getName(), AccountTypeUtils.getAuthTokenTypeSamlSessionCookie(account.getType()));
            credentials= OwnCloudCredentialsFactory.newSamlSsoCredentials(username, accessToken);
        }else{
            String password= am.getAuthToken(account.getName(), AccountTypeUtils.getAuthTokenTypePass(account.getType()));
            credentials= OwnCloudCredentialsFactory.newBasicCredentials(username, password);
        }

        return credentials;
    }

    public static String buildAccountNameOld(URI serverBaseUrl, String username){
        if(serverBaseUrl.getScheme() == null)
            serverBaseUrl= URI.create("https://" + serverBaseUrl.toString());
        String accountName= username + "@" + serverBaseUrl.getHost();
        if(serverBaseUrl.getPort() >= 0)
            accountName+= ":" + serverBaseUrl.getPort();
        return accountName;
    }

    public static String buildAccountName(URI serverBaseUrl, String username){
        if(serverBaseUrl.getScheme() == null)
            serverBaseUrl= URI.create("https://" + serverBaseUrl.toString());

        // Remove http:// or https://
        String url= serverBaseUrl.toString();
        if(url.contains("://"))
            url= url.substring(serverBaseUrl.toString().indexOf("://") + 3);
        String accountName= username + "@" + url;

        return accountName;
    }

    public static void saveClient(AccountManager am, OwnCloudClient client, Account savedAccount){
        if(client != null){
            String cookiesString= client.getCookiesString();
            if(!"".equals(cookiesString)){
                savedAccount.setCookies(cookiesString);
                am.saveAccount(savedAccount);
            }
        }
    }

    /**
    * Restore the client cookies
    * @param account
    * @param client
    * @param context
    */
    public static void restoreCookies(Account account, OwnCloudClient client){
        Log_OC.d(TAG, "Restoring cookies for " + account.getName());

        URI serverUri= client.getBaseUri() != null ? client.getBaseUri() : client.getWebdavUri();
        String cookiesString= account.getCookies();
        if(cookiesString != null){
            String[] cookies= cookiesString.split(";");
            if(cookies.length > 0)
                for(int i= 0; i < cookies.length; i++){
                    Cookie cookie= new Cookie();
                    int equalPos= cookies[i].indexOf('=');
                    cookie.setName(cookies[i].substring(0, equalPos));
                    cookie.setValue(cookies[i].substring(equalPos + 1));
                    cookie.setDomain(serverUri.getHost()); // VERY IMPORTANT
                    cookie.setPath(serverUri.getPath()); // VERY IMPORTANT

                    client.getState().addCookie(cookie);
                }
        }
    }

    public static class AccountNotFoundException extends AccountsException{

        /** Generated - should be refreshed every time the class changes!! */
        private static final long serialVersionUID= -1684392454798508693L;

        private Account mFailedAccount;

        public AccountNotFoundException(Account failedAccount, String message, Throwable cause){
            super(message, cause);
            mFailedAccount= failedAccount;
        }

        public Account getFailedAccount(){
            return mFailedAccount;
        }
    }

}
