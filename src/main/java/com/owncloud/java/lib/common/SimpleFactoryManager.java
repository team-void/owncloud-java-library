package com.owncloud.java.lib.common;

import java.io.IOException;

import com.owncloud.java.lib.account.AccountManager;
import com.owncloud.java.lib.common.accounts.AccountUtils;
import com.owncloud.java.lib.common.accounts.AccountUtils.AccountNotFoundException;
import com.owncloud.java.lib.common.utils.Log_OC;
import com.owncloud.java.lib.env.LibraryContext;
import com.owncloud.java.lib.exceptions.AuthenticatorException;
import com.owncloud.java.lib.exceptions.OperationCanceledException;

public class SimpleFactoryManager implements OwnCloudClientManager{

    private static final String TAG= SimpleFactoryManager.class.getSimpleName();

    @Override
    public OwnCloudClient getClientFor(OwnCloudAccount account, LibraryContext context)
            throws AccountNotFoundException, OperationCanceledException, AuthenticatorException,
            IOException{

        Log_OC.d(TAG, "getClientFor(OwnCloudAccount ... : ");
        OwnCloudClient client= OwnCloudClientFactory.createOwnCloudClient(account.getBaseUri(), context, false);

        Log_OC.v(TAG, "    new client {" +
                (account.getName() != null ? account.getName() : AccountUtils.buildAccountName(account.getBaseUri(), "")

                ) + ", " + client.hashCode() + "}");

        if(account.getCredentials() == null)
            account.loadCredentials(context.getAccountManager());
        client.setCredentials(account.getCredentials());
        return client;
    }

    @Override
    public OwnCloudClient removeClientFor(OwnCloudAccount account){
        // nothing to do - not taking care of tracking instances!
        return null;
    }

    @Override
    public void saveAllClients(AccountManager am, String accountType){
        // nothing to do - not taking care of tracking instances!
    }

}
