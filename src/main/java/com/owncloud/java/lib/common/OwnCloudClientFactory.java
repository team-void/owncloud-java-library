/*
 * ownCloud Android Library is available under MIT license Copyright (C) 2016
 * ownCloud GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.owncloud.java.lib.common;

import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;

import com.owncloud.java.lib.account.AccountManager;
import com.owncloud.java.lib.common.accounts.AccountTypeUtils;
import com.owncloud.java.lib.common.accounts.AccountUtils;
import com.owncloud.java.lib.common.accounts.AccountUtils.AccountNotFoundException;
import com.owncloud.java.lib.common.network.NetworkUtils;
import com.owncloud.java.lib.common.utils.Log_OC;
import com.owncloud.java.lib.env.Account;
import com.owncloud.java.lib.env.LibraryContext;
import com.owncloud.java.lib.exceptions.AuthenticatorException;
import com.owncloud.java.lib.exceptions.OperationCanceledException;

public class OwnCloudClientFactory{

    final private static String TAG= OwnCloudClientFactory.class.getSimpleName();

    /** Default timeout for waiting data from the server */
    public static final int DEFAULT_DATA_TIMEOUT= 60000;

    /** Default timeout for establishing a connection */
    public static final int DEFAULT_CONNECTION_TIMEOUT= 60000;

    /**
     * Creates a OwnCloudClient setup for an ownCloud account
     *
     * Do not call this method from the main thread.
     *
     * @param account                       The ownCloud account
     * @param context   Context object with the current environment information
     * @return                              A OwnCloudClient object ready to be used
     * @throws AuthenticatorException       If the authenticator failed to get the authorization
     *                                      token for the account.
     * @throws OperationCanceledException   If the authenticator operation was cancelled while
     *                                      getting the authorization token for the account.
     * @throws IOException                  If there was some I/O error while getting the
     *                                      authorization token for the account.
     * @throws AccountNotFoundException     If 'account' is unknown for the AccountManager
     */
    public static OwnCloudClient createOwnCloudClient(AccountManager am, Account account, LibraryContext context)
            throws OperationCanceledException, AuthenticatorException, IOException,
            AccountNotFoundException{

        URI baseUri= account.getOCBaseURL();
        boolean isOauth2= account.hasOAuth2Support();
        boolean isSamlSso= account.hasSAML_WEB_SSOSupport();
        OwnCloudClient client= createOwnCloudClient(baseUri, context, !isSamlSso);

        String username= account.getName();
        if(isOauth2){
            String accessToken= am.getAuthToken(account.getName(),
                    AccountTypeUtils.getAuthTokenTypeAccessToken(account.getType()));

            client.setCredentials(OwnCloudCredentialsFactory.newBearerCredentials(accessToken));

        }else if(isSamlSso){
            String accessToken= am.getAuthToken(account.getName(),
                    AccountTypeUtils.getAuthTokenTypeSamlSessionCookie(account.getType()));

            client.setCredentials(OwnCloudCredentialsFactory.newSamlSsoCredentials(username, accessToken));

        }else{
            String password= am.getAuthToken(account.getName(),
                    AccountTypeUtils.getAuthTokenTypePass(account.getType()));

            client.setCredentials(OwnCloudCredentialsFactory.newBasicCredentials(username, password));

        }

        // Restore cookies
        AccountUtils.restoreCookies(account, client);

        return client;
    }

    /**
     * Creates a OwnCloudClient to access a URL and sets the desired parameters for ownCloud
     * client connections.
     *
     * @param uri       URL to the ownCloud server; BASE ENTRY POINT, not WebDavPATH
     * @param context   Context object with the current environment information
     * @return          A OwnCloudClient object ready to be used
     */
    public static OwnCloudClient createOwnCloudClient(URI uri, LibraryContext context, boolean followRedirects){
        try{
            NetworkUtils.registerAdvancedSslContext(true, context.getKeyStorePath());
        }catch(GeneralSecurityException e){
            Log_OC.e(TAG, "Advanced SSL Context could not be loaded. Default SSL management in" +
                    " the system will be used for HTTPS connections", e);

        }catch(IOException e){
            Log_OC.e(TAG, "The local server truststore could not be read. Default SSL management" +
                    " in the system will be used for HTTPS connections", e);
        }

        OwnCloudClient client= new OwnCloudClient(uri, NetworkUtils.getMultiThreadedConnManager());
        client.setDefaultTimeouts(DEFAULT_DATA_TIMEOUT, DEFAULT_CONNECTION_TIMEOUT);
        client.setFollowRedirects(followRedirects);

        return client;
    }

}
