package com.owncloud.java.lib.common.network;

public interface OnDatatransferProgressListener{

    public void onTransferProgress(long progressRate, long totalTransferredSoFar, long totalToTransfer, String fileAbsoluteName);
}
