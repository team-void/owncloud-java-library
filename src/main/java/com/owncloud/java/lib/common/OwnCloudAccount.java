package com.owncloud.java.lib.common;

import java.io.IOException;
import java.net.URI;

import com.owncloud.java.lib.account.AccountManager;
import com.owncloud.java.lib.common.accounts.AccountUtils;
import com.owncloud.java.lib.common.accounts.AccountUtils.AccountNotFoundException;
import com.owncloud.java.lib.env.Account;
import com.owncloud.java.lib.exceptions.AuthenticatorException;
import com.owncloud.java.lib.exceptions.OperationCanceledException;

/**
 * OwnCloud Account
 *
 * @author David A. Velasco
 */
public class OwnCloudAccount{

    private URI mBaseUri;

    private OwnCloudCredentials mCredentials;

    private String mDisplayName;

    private String mSavedAccountName;

    private Account mSavedAccount;

    /**
     * Constructor for already saved OC accounts.
     *
     * Do not use for anonymous credentials.
     */
    public OwnCloudAccount(Account savedAccount) throws AccountNotFoundException{
        if(savedAccount == null)
            throw new IllegalArgumentException("Parameter 'savedAccount' cannot be null");

        mSavedAccount= savedAccount;
        mSavedAccountName= savedAccount.getName();
        mCredentials= null; // load of credentials is delayed
        mBaseUri= savedAccount.getOCBaseURL();
        mDisplayName= savedAccount.getDisplayName();
    }

    /**
     * Constructor for non yet saved OC accounts.
     *
     * @param baseUri           URI to the OC server to get access to.
     * @param credentials       Credentials to authenticate in the server. NULL is valid for anonymous credentials.
     */
    public OwnCloudAccount(URI baseUri, OwnCloudCredentials credentials){
        if(baseUri == null)
            throw new IllegalArgumentException("Parameter 'baseUri' cannot be null");
        mSavedAccount= null;
        mSavedAccountName= null;
        mBaseUri= baseUri;
        mCredentials= credentials != null ? credentials : OwnCloudCredentialsFactory.getAnonymousCredentials();
        String username= mCredentials.getUsername();
        if(username != null)
            mSavedAccountName= AccountUtils.buildAccountName(mBaseUri, username);
    }

    /**
     * Method for deferred load of account attributes from AccountManager
     *
     * @throws AccountNotFoundException
     * @throws AuthenticatorException
     * @throws IOException
     * @throws OperationCanceledException
     */
    public void loadCredentials(AccountManager am)
            throws AccountNotFoundException, AuthenticatorException,
            IOException, OperationCanceledException{

        if(mSavedAccount != null)
            mCredentials= AccountUtils.getCredentialsForAccount(am, mSavedAccount);
    }

    public URI getBaseUri(){
        return mBaseUri;
    }

    public OwnCloudCredentials getCredentials(){
        return mCredentials;
    }

    public String getName(){
        return mSavedAccountName;
    }

    public String getDisplayName(){
        if(mDisplayName != null && mDisplayName.length() > 0)
            return mDisplayName;
        else if(mCredentials != null)
            return mCredentials.getUsername();
        else if(mSavedAccount != null)
            return mSavedAccount.getDisplayName();
        else
            return null;
    }

}