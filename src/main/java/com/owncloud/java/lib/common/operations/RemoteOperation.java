/*
 * ownCloud Android Library is available under MIT license Copyright (C) 2016
 * ownCloud GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.owncloud.java.lib.common.operations;

import java.io.IOException;
import java.util.concurrent.Executor;

import com.owncloud.java.lib.common.OwnCloudAccount;
import com.owncloud.java.lib.common.OwnCloudClient;
import com.owncloud.java.lib.common.OwnCloudClientManagerFactory;
import com.owncloud.java.lib.common.accounts.AccountUtils;
import com.owncloud.java.lib.common.utils.Log_OC;
import com.owncloud.java.lib.env.Account;
import com.owncloud.java.lib.env.LibraryContext;
import com.owncloud.java.lib.exceptions.AccountsException;
import com.owncloud.java.lib.exceptions.AuthenticatorException;
import com.owncloud.java.lib.exceptions.OperationCanceledException;

/**
 * Operation which execution involves one or several interactions with an ownCloud server.
 *
 * Provides methods to execute the operation both synchronously or asynchronously.
 *
 * @author David A. Velasco
 */
public abstract class RemoteOperation implements Runnable{

    private static final String TAG= RemoteOperation.class.getSimpleName();

    /** OCS API header name */
    public static final String OCS_API_HEADER= "OCS-APIREQUEST";

    /** OCS API header value */
    public static final String OCS_API_HEADER_VALUE= "true";

    /**  */
    private LibraryContext mContext= null;

    /** ownCloud account in the remote ownCloud server to operate */
    private Account mAccount= null;

    /** Object to interact with the remote server */
    private OwnCloudClient mClient= null;

    /** Callback object to notify about the execution of the remote operation */
    private OnRemoteOperationListener mListener= null;

    /** Handler to the thread where mListener methods will be called */
    private Executor mThreadExecutor= null;

    /**
     *  Abstract method to implement the operation in derived classes.
     */
    protected abstract RemoteOperationResult run(OwnCloudClient client);

    /**
     * Synchronously executes the remote operation on the received ownCloud account.
     *
     * Do not call this method from the main thread.
     *
     * This method should be used whenever an ownCloud account is available, instead of
     * {@link #execute(OwnCloudClient)}.
     *
     * @param account   ownCloud account in remote ownCloud server to reach during the
     *                  execution of the operation.
     * @return          Result of the operation.
     */
    public RemoteOperationResult execute(Account account){
        if(account == null)
            throw new IllegalArgumentException("Trying to execute a remote operation with a NULL Account");
        mAccount= account;
        try{
            OwnCloudAccount ocAccount= new OwnCloudAccount(mAccount);
            mClient= OwnCloudClientManagerFactory.getDefaultSingleton().getClientFor(ocAccount, mContext);
        }catch(Exception e){
            Log_OC.e(TAG, "Error while trying to access to " + mAccount.getName(), e);
            return new RemoteOperationResult(e);
        }
        return run(mClient);
    }

    /**
     * Synchronously executes the remote operation
     *
     * Do not call this method from the main thread.
     *
     * @param client	Client object to reach an ownCloud server during the execution of
     *                  the operation.
     * @return			Result of the operation.
     */
    public RemoteOperationResult execute(OwnCloudClient client){
        if(client == null)
            throw new IllegalArgumentException("Trying to execute a remote operation with a NULL OwnCloudClient");
        mClient= client;
        return run(mClient);
    }

    /**
     * Asynchronously executes the remote operation
     *
     * This method should be used whenever an ownCloud account is available,
     * instead of {@link #execute(OwnCloudClient, OnRemoteOperationListener, Handler))}.
     *
     * @param account           ownCloud account in remote ownCloud server to reach during the
     * 							execution of the operation.
     * @param listener          Listener to be notified about the execution of the operation.
     * @param threadExecutor    Executor, if passed in, associated to the thread where the methods of
     *                          the listener objects must be called.
     * @return                  Thread were the remote operation is executed.
     */
    public Thread execute(Account account, OnRemoteOperationListener listener, Executor threadExecutor){
        if(account == null)
            throw new IllegalArgumentException("Trying to execute a remote operation with a NULL Account");

        mAccount= account;
        // the client instance will be created from mAccount in the runnerThread
        // to create below
        mClient= null;

        mListener= listener;
        mThreadExecutor= threadExecutor;

        Thread runnerThread= new Thread(this);
        runnerThread.start();
        return runnerThread;
    }

    /**
     * Asynchronously executes the remote operation
     *
     * @param client			Client object to reach an ownCloud server
     *                          during the execution of the operation.
     * @param listener			Listener to be notified about the execution of the operation.
     * @param threadExecutor	Executor, if passed in, associated to the thread where the methods of
     *                          the listener objects must be called.
     * @return					Thread were the remote operation is executed.
     */
    public Thread execute(OwnCloudClient client, OnRemoteOperationListener listener, Executor threadExecutor){
        if(client == null)
            throw new IllegalArgumentException("Trying to execute a remote operation with a NULL OwnCloudClient");
        mClient= client;

        if(listener == null)
            throw new IllegalArgumentException("Trying to execute a remote operation asynchronously " +
                    "without a listener to notiy the result");
        mListener= listener;

        if(threadExecutor != null)
            mThreadExecutor= threadExecutor;

        Thread runnerThread= new Thread(this);
        runnerThread.start();
        return runnerThread;
    }

    /**
     * Asynchronous execution of the operation
     * started by {@link RemoteOperation#execute(OwnCloudClient, OnRemoteOperationListener, Executor)},
     * and result posting.
     *
     */
    @Override
    public final void run(){
        RemoteOperationResult result= null;
        String accountName= mAccount != null ? mAccount.getName() : "Account is NULL";
        try{
            if(mClient == null)
                if(mAccount != null){
                    OwnCloudAccount ocAccount= new OwnCloudAccount(mAccount);
                    mClient= OwnCloudClientManagerFactory.getDefaultSingleton().getClientFor(ocAccount, mContext);
                }else
                    throw new IllegalStateException("Trying to run a remote operation " +
                            "asynchronously with no client instance or account");

        }catch(IOException e){
            Log_OC.e(TAG, "Error while trying to access to " + accountName,
                    new AccountsException("I/O exception while trying to authorize the account", e));
            result= new RemoteOperationResult(e);
        }catch(AccountsException e){
            Log_OC.e(TAG, "Error while trying to access to " + accountName, e);
            result= new RemoteOperationResult(e);
        }catch(OperationCanceledException e){
            Log_OC.e(TAG, "OperationCanceledException. Account name: " + accountName, e);
            result= new RemoteOperationResult(e);
        }catch(AuthenticatorException e){
            Log_OC.e(TAG, "AuthenticatorException. Account name: " + accountName, e);
            result= new RemoteOperationResult(e);
        }

        if(result == null)
            result= run(mClient);

        if(mAccount != null)
            // Save Client Cookies
            AccountUtils.saveClient(mContext.getAccountManager(), mClient, mAccount);

        final RemoteOperationResult resultToSend= result;
        if(mThreadExecutor != null && mListener != null)
            mThreadExecutor.execute(new Runnable(){

                @Override
                public void run(){
                    mListener.onRemoteOperationFinish(RemoteOperation.this, resultToSend);
                }
            });
        else if(mListener != null)
            mListener.onRemoteOperationFinish(RemoteOperation.this, resultToSend);
    }

    /**
     * Returns the current client instance to access the remote server.
     *
     * @return      Current client instance to access the remote server.
     */
    public final OwnCloudClient getClient(){
        return mClient;
    }

}
